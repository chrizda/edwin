@extends('admin/templateadmin')
@section('judul')
    Halaman - Peramalan Limbah
@endsection
@section('css')

    <link rel="stylesheet" href="../vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="../vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="../vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="../vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">

    <link rel="stylesheet" href="../assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

@endsection
@section('content')

    <div class="content mt-3">
        @if($pesan = Session::get('alert'))
            <a href="/peramalan">
                <div class="alert alert-success fade show" role="alert">
                    {{ $pesan }}
                </div>
            </a>
        @endif
        @if($pesan = Session::get('error'))
            <a href="/peramalan">
                <div class="alert alert-success fade show" role="alert">
                    {{ $pesan }}
                </div>
            </a>
        @endif

        <div class="animated fadeIn">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header text-center">
                            <strong class="card-title">Buat Peramalan</strong>
                            <br>
                            <form action="/buatperamalan" method="post" class="">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <div class="input-group col-md-3">
                                        <div class="input-group-addon"><i class="fa fa-truck"></i></div>
                                        <select id="bahanbaku" name="bahanbaku" class="form-control" required>
                                            <option value="">Pilih Bahan Baku</option>
                                            <option value="1">Limbah Jagung</option>
                                            <option value="2">Limbah Padi</option>
                                        </select>
                                    </div>
                                    <div class="input-group col-md-3">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <select id="bulan" name="bulan" class="form-control" required>
                                            <option value="">Pilih Bulan</option>
                                            @for($i=0; $i<12; $i++)
                                                <option value="{{ $bln['nomer'][$i] }}">{{ $bln['nama'][$i] }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="input-group col-md-3">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <select id="tahun" name="tahun" class="form-control" required>
                                            <option value="">Pilih Tahun</option>
                                            @for($t=2017; $t <= date('Y', strtotime('+1 year', strtotime(date('Y')))); $t++)
                                                <option value="{{ $t }}">{{ $t }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-md-1">

                                    </div>
                                    <div class="input-group col-md-1">
                                        <button type="submit" class="btn btn-success btn-sm">Submit</button>
                                    </div>
                                    <div class="input-group col-md-1">
                                        <a class="btn btn-danger btn-sm" role="button" href="/peramalan">Reset</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                </div>
            </div>

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Histori Peramalan</strong>
                        </div>
                        <div class="card-body">
                                <table id="bootstrap-data-table-export" class="table table-bordered">
                                    <thead align="center">
                                    <tr>
                                        <th>#</th>
                                        <th>Bahanbaku</th>
                                        <th>Tanggal</th>
                                        <th>Periode</th>
                                        <th>Aktual</th>
                                        <th>Hasil</th>
                                        <th>Alpha</th>
                                        <th>PE</th>
                                        <th>MAPE</th>
                                        <th>Safety Stock</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $no = 1 ?>
                                    @foreach($peramalan as $p)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $p->nama_bahanbaku }}</td>
                                            <td>{{ date('d-m-Y', strtotime($p->tanggal_peramalan)) }}</td>
                                            <td>{{ date('M Y', strtotime($p->periode)) }}</td>
                                            <td>{{ $p->aktual }}</td>
                                            <td>{{ $p->hasil_peramalan }}</td>
                                            <td>{{ $p->alpha }}</td>
                                            <td>{{ $p->PE }}</td>
                                            <td>{{ $p->MAPE }}</td>
                                            <td>{{ $p->safety_stock }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                        </div>
                    </div>
                </div>

        </div><!-- .animated -->
    </div><!-- .content -->


    <!-- Right Panel -->

    <div class="modal fade" id="tambahdataperamalan" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticModalLabel">Tambah Data Peramalan</h5>
                </div>
                <form action="/buatperamalan" method="post" class="">
                    {{ csrf_field() }}
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-truck"></i></div>
                                <select id="bahanbaku" name="bahanbaku" class="form-control" required>
                                    <option value="">--- Pilih Bahan Baku ---</option>
                                    @foreach($bahanbaku as $bb)
                                        <option value="{{ $bb->id_bahanbaku }}">{{ $bb->id_bahanbaku.'. '.$bb->nama_bahanbaku }}</option>
                                    @endforeach
                                </select>
                                {{--<input type="text" id="bahanbaku" name="bahanbaku" placeholder="Pilih Bahan Baku" class="form-control">--}}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <select id="bulan" name="bulan" class="form-control" required>
                                    <option value="">--- Pilih Bulan Periode---</option>
                                    @for($i=0; $i<count($bln['nomer']); $i++)
                                        <option value="{{ $bln['nomer'][$i] }}">{{ $bln['nama'][$i] }}</option>
                                    @endfor
                                </select>
                                {{--<input type="date" id="bulan" name="bulan" placeholder="Bulan" class="form-control">--}}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <select id="tahun" name="tahun" class="form-control" required>
                                    <option value="">--- Pilih Tahun Periode ---</option>
                                    @for($t=2017; $t <= date('Y', strtotime('+1 year', strtotime(date('Y')))); $t++)
                                        <option value="{{ $t }}">{{ $t }}</option>
                                    @endfor
                                </select>
                                {{--<input type="date" id="periode" name="periode" placeholder="Periode" class="form-control">--}}
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-success btn-sm">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <script src="../vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../assets/js/main.js"></script>


    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <script src="../assets/js/init-scripts/data-table/datatables-init.js"></script>

@endsection
