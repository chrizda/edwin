@extends('admin/templateadmin')
@section('judul')
    Halaman - Gudang Bahan Baku
@endsection
@section('css')

    <link rel="stylesheet" href="../vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="../vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="../vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="../vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">

    <link rel="stylesheet" href="../assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

@endsection
@section('content')

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Data Gudang Bahan Baku</strong>
                            <div class="float-right">
                                <button type="button" class="btn btn-info mb-1 btn-sm" data-toggle="modal" data-target="#tambahdatagudang">Tambah Data</button>
                            </div>
                        </div>
                        <div class="card-body">
                            @if(\Session::has('alert'))
                                <div class="alert alert-success text-center">
                                    <div>{{Session::get('alert')}}</div>
                                </div>
                            @endif
                                @if(\Session::has('error'))
                                    <div class="alert alert-danger text-center">
                                        <div>{{Session::get('error')}}</div>
                                    </div>
                                @endif
                            <table id="bootstrap-data-table-export" class="table table-bordered">
                                <thead align="center">
                                <tr>
                                    <th width="10px">#</th>
                                    <th width="100px">Bahan Baku</th>
                                    <th width="10%">Aksi</th>
                                    <th width="70px">Stok</th>
                                    {{--<th width="500px">Status</th>--}}
                                    <th width="10%">Aksi</th>
                                    <th width="10%">Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $no = 1; @endphp
                                @foreach($dataStok as $a)
                                    <tr>
                                        <td>{{$no}}</td>
                                        <td>{{$a->nama_bahanbaku}}</td>
                                        <td class="text-center">
                                            <button type="button" class="tambahkecart btn btn-success fa fa-shopping-cart" data-idbb="{{$a->id_bahanbaku}}" data-nama="{{$a->nama_bahanbaku}}" data-sat="{{$a->satuan}}" data-jumlah="{{$a->jumlah}}"></button>
                                        </td>
                                        <td>{{$a->jumlah}} {{$a->satuan}}</td>
                                        {{--<td>--}}
                                            {{--<div class="progress mb-2" style="height: 25px;">--}}
                                                {{--<div class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" style="width: 50%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"><strong> Safety Stock 0 {{$a->satuan}} </strong>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        {{--</td>--}}
                                        <td>
                                            <button type="button" class="btntambahstok btn btn-warning btn-sm" data-idbb="{{$a->id_bahanbaku}}" data-nama="{{$a->nama_bahanbaku}}"  data-toggle="modal" data-target="#tambahstok">Tambah Stok</button>
                                        </td>
                                        {{--@if($a->bahanbaku_id=="1" ||$a->bahanbaku_id=="2" ||$a->bahanbaku_id=="3")--}}
                                            {{--<td></td>--}}
                                        {{--@else--}}
                                            <td class="text-center">
                                                <a href="{{route('delete',$a->bahanbaku_id)}}" type="button" class="btn btn-danger fa fa-trash btn-sm" onclick="return confirm('Apakah anda yakin menghapus bahan baku ini?')"></a>
                                            </td>
                                        {{--@endif--}}

                                    </tr>
                                @php $no++; @endphp
                                @endforeach
{{--                                <tr>--}}
{{--                                    <th>1</th>--}}
{{--                                    <td>--}}
{{--                                        Limbah Jagung--}}
{{--                                    </td>--}}
{{--                                    <td align="center">--}}
{{--                                        <h5>527 Kg</h5>--}}
{{--                                    </td>--}}
{{--                                    <td>--}}
{{--                                        <div class="progress mb-2" style="height: 25px;">--}}
{{--                                            <div class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" style="width: 50%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"><strong> Safety Stock 224 Kg </strong>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </td>--}}
{{--                                    <td>--}}
{{--                                        <button type="button" class="btn btn-warning btn-sm" >Tambah Stok</button>--}}
{{--                                    </td>--}}
{{--                                    <td>--}}
{{--                                        <button type="button" class="btn btn-success fa fa-truck btn-sm" ></button>--}}
{{--                                        <button type="button" class="btn btn-danger fa fa-trash btn-sm" ></button>--}}
{{--                                    </td>--}}
{{--                                </tr>--}}
                                </tbody>
                            </table>
                        </div>
                        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#kirimbahanbaku"><strong>Kirim Bahan Baku</strong></button>
                    </div>

                </div>


            </div>
        </div><!-- .animated -->
    </div><!-- .content -->


    <!-- Right Panel -->

    <div class="modal fade" id="tambahdatagudang" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticModalLabel">Tambah Data</h5>
                </div>
                <div class="modal-body">

                    <form action="/tambahBahanBakuAdmin" method="post" class="">

                        {{csrf_field()}}
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-book"></i></div>
                                <input type="text" id="namabb" name="namabb" placeholder="Bahan Baku" class="form-control">
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-plus"></i></div>
                                <input type="text" id="stok" name="stok" placeholder="Stok" class="form-control">
                            </div>
                        </div>

                        {{--<div class="form-group">--}}
                            {{--<div class="input-group">--}}
                                {{--<div class="input-group-addon"><i class="fa fa-shopping-bag"></i></div>--}}
                                <input type="hidden" id="sat" name="sat" placeholder="Satuan" class="form-control" value="Kg">
                            {{--</div>--}}
                        {{--</div>--}}

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success btn-sm">Submit</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="tambahstok" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="judulstok">Tambah Stok <span id="namabb"></span></h5>
                </div>
                <div class="modal-body">

                    <form action="/tambahStokAdmin" method="post">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"><i class="fa fa-plus"></i></div>
                                {{csrf_field()}}
                                <input type="hidden" id="idbb" name="idbb" value="">
                                <input type="text" id="tambahan" name="tambahan" placeholder="Jumlah Tambahan Stok" class="form-control" onkeypress="return hanyaAngka(event)">
                            </div>
                        </div>



                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success btn-sm">Submit</button>
                </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal fade" id="kirimbahanbaku" tabindex="-1" role="dialog" aria-labelledby="staticModalLabel" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
                <form method="post" action="/kirimBahanBakuAdmin">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticModalLabel">Form Kirim Bahan Baku</h5>

                </div>
                <div class="modal-body">
                    <table class="table table-bordered">
                        <thead align="center">
                        <tr>
                            <th >Limbah Tani</th>
                            <th >Stok (Kg)</th>
                            <th width="10%">Aksi</th>
                        </tr>
                        </thead>
                        <tbody id="isicart">

                            {{csrf_field()}}

{{--                        <tr>--}}
{{--                            <td>--}}
{{--                                Limbah Jagung--}}
{{--                            </td>--}}
{{--                            <td><center>--}}
{{--                                    <input size="5" type="text" maxlength="4" onkeypress="return hanyaAngka(event)"/>--}}
{{--                                </center>--}}
{{--                            </td>--}}
{{--                            <td>--}}
{{--                                <button type="button" class="btn btn-danger btn-sm" >Hapus</button>--}}
{{--                            </td>--}}
{{--                        </tr>--}}
{{--                        <tr>--}}
{{--                            <td>--}}
{{--                                Limbah Padi--}}
{{--                            </td>--}}
{{--                            <td><center>--}}
{{--                                    <input size="5" type="text" maxlength="4" onkeypress="return hanyaAngka(event)"/>--}}
{{--                                </center>--}}
{{--                            </td>--}}
{{--                            <td>--}}
{{--                                <button type="button" class="btn btn-danger btn-sm" >Hapus</button>--}}
{{--                            </td>--}}
{{--                        </tr>--}}
{{--                        <tr>--}}
{{--                            <td>--}}
{{--                                Limbah Tebu--}}
{{--                            </td>--}}
{{--                            <td><center>--}}
{{--                                    <input size="5" type="text" maxlength="4" onkeypress="return hanyaAngka(event)"/>--}}

{{--                                </center>--}}
{{--                            </td>--}}
{{--                            <td>--}}
{{--                                <button type="button" class="btn btn-danger btn-sm" >Hapus</button>--}}
{{--                            </td>--}}
{{--                        </tr>--}}
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Kembali</button>
                    <button type="submit" class="btn btn-success btn-sm">Submit</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <script src="../vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../assets/js/main.js"></script>
    <script>
        jQuery(document).ready(function($){
            $(".btntambahstok").click(function () {
                $('#judulstok').text('Tambah Stok '+$(this).data('nama'));
                $("#idbb").val($(this).data('idbb'));
                $("#tambahstok").modal("show");
            });

            $(".tambahkecart").click(function(){
                var jumlah = $(this).data('jumlah');
                if(jumlah == "0"){
                    alert("Stok bahan baku ini kosong");
                }else {
                    var id = $(this).data('idbb');
                    var nama = $(this).data('nama');
                    var sat = $(this).data('sat');
                    if ($('#bb-' + id).length) {
                        alert('Data sudah masuk di form kirim bahan baku')
                    } else {
                        $("#isicart").append("<tr id=\"bb-" + id + "\"><td><input name=\"idbb[]\" type=\"hidden\" value=\"" + id + "\"><input name=\"sat[]\" type=\"hidden\" value=\"" + sat + "\">" + nama + "</td><td><input name=\"qty[]\" size=\"5\" type=\"text\" maxlength=\"4\" onkeypress=\"return hanyaAngka(event)\"/></td>" +
                            "<td><button type=\"button\" class=\"hapuscart btn btn-danger btn-sm\" data-id=\"" + id + "\" >Hapus</button></td>");
                        $(".hapuscart").click(function () {
                            var id = $(this).data('id');
                            $("#bb-" + id).fadeOut();
                            $("#bb-" + id).remove();
                        })
                    }
                }
            });


        });
    </script>
    <script>
        function hanyaAngka(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            return !(charCode > 31 && (charCode < 48 || charCode > 57));
        }
    </script>



    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <script src="../assets/js/init-scripts/data-table/datatables-init.js"></script>

@endsection
