@extends("admin/templateadmin")
@section('judul')
    Halaman - Riwayat Pengiriman
@endsection
@section('css')
    <link rel="stylesheet" href="../vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="../vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="../vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="../vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">

    <link rel="stylesheet" href="../assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
@endsection

@section('content')

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Data Pengiriman Limbah Tani</strong>
                                   
                            </div>
                            <div class="card-body">
                                <table id="riwayatpengiriman" class="table table-bordered">
                                    <thead align="center">
                                        <tr>
                                            <th width="10px">#</th>
                                            <th width="300px">Transaksi</th>
                                            <th>Tanggal Kirim</th>
                                               
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @php if(!empty($a)){$b = $a->getContent(); $c = json_decode($b);}else{$c="Kosong";} $no=1; @endphp
                                    @if($c!="Kosong")
                                    @foreach($c as $trans)
{{--                                        @php echo $trans->tanggal;@endphp--}}
{{--                                        @php var_dump($trans->trans);@endphp--}}
                                        <tr>
                                        <th class="text-center" style="vertical-align: middle">{{$no}}</th>
                                            <td>
                                                <div class="col-12" style="vertical-align: middle">
                                                    <ol>
                                                        @foreach($trans->trans as $detail)
                                                            <li>
                                                                {{$detail->nama_bahanbaku}} <span class="badge badge-secondary pull-right">{{$detail->jumlah}} {{$detail->satuan}}</span>
                                                            </li>
                                                            @if(!$loop->last)
                                                                @if(count($trans->trans)!=1)
                                                                    <hr>
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    </ol>
                                                </div>
                                            </td>
                                            <td class="text-center" style="vertical-align: middle;">
                                                @php echo date('d/M/Y',strtotime($trans->tanggal));@endphp
                                            </td>
                                        </tr>

                                        @php $no++; @endphp
                                    @endforeach
                                    @endif
{{--                                        <tr>--}}
{{--                                            <th>1</th>--}}
{{--                                            <td>--}}
{{--                                                <div class="col-12">--}}
{{--                                                    <ol>--}}
{{--                                                        <li>--}}
{{--                                                             Limbah Padi <span class="badge badge-secondary pull-right">100 Kg</span>--}}
{{--                                                         --}}
{{--                                                        </li>--}}
{{--                                                        <li>--}}
{{--                                                            <hr>--}}
{{--                                                             Limbah Jagung <span class="badge badge-secondary pull-right">150 Kg</span>--}}
{{--                                                         --}}
{{--                                                        </li>--}}
{{--                                                        <li>--}}
{{--                                                            <hr>--}}
{{--                                                            Limbah Tebu<span class="badge badge-secondary pull-right">110 Kg</span>--}}
{{--                                                            --}}
{{--                                                        </li>--}}
{{--                                                    </ol>--}}
{{--                                                </div>--}}
{{--                                            </td>--}}
{{--                                            <td align="center"> --}}
{{--                                            <br>--}}
{{--                                            <br>--}}
{{--                                            <br>--}}
{{--                                                <h5>12/02/19</h5> --}}
{{--                                            </td>--}}
{{--                                            --}}
{{--                                           --}}
{{--                                        </tr>--}}
                                    </tbody>
                                </table>

                            </div>
                             
                        </div>

                    </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->


    </div><!-- /#right-panel -->

    <!-- Right Panel -->
@endsection
@section('js')
   

    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <script src="../vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../assets/js/main.js"></script>


    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <script src="../assets/js/init-scripts/data-table/datatables-init.js"></script>
    <script>
        jQuery("#riwayatpengiriman").dataTable({
            "language":{
                "emptyTable": "Tidak ada riwayat pengiriman"
            }
        })
    </script>
@endsection
