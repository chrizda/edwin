@extends('admin/templateadmin')
@section('judul')
    Halaman - Peramalan Limbah
@endsection
@section('css')

    <link rel="stylesheet" href="../vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="../vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="../vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="../vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">

    <link rel="stylesheet" href="../assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

@endsection
@section('content')

    <div class="content mt-3">
        <div class="animated fadeIn">
            <div class="row">

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header text-center">
                            <strong class="card-title">Buat Peramalan</strong>
                            <br>
                            <form action="/buatperamalan" method="post" class="">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <div class="input-group col-md-3">
                                        <div class="input-group-addon"><i class="fa fa-truck"></i></div>
                                        <select id="bahanbaku" name="bahanbaku" class="form-control" required>
                                            <option value="">Pilih Bahan Baku</option>
                                            <option value="1" <?php if ($bb['id_bahanbaku'] == 1){ echo "selected";} ?>>Limbah Jagung</option>
                                            <option value="2"<?php if ($bb['id_bahanbaku'] == 2){ echo "selected";} ?>>Limbah Padi</option>
                                        </select>
                                    </div>
                                    <div class="input-group col-md-3">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <select id="bulan" name="bulan" class="form-control" required>
                                            <option value="">Pilih Bulan</option>
                                            @for($i=0; $i<12; $i++)
                                                <option value="{{ $bln['nomer'][$i] }}" <?php if ($bln['nomer'][$i] == $bulan){ echo "selected";} ?>>{{ $bln['nama'][$i] }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="input-group col-md-3">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                        <select id="tahun" name="tahun" class="form-control" required>
                                            <option value="">Pilih Tahun</option>
                                            @for($t=2017; $t <= date('Y', strtotime('+1 year', strtotime(date('Y')))); $t++)
                                                <option value="{{ $t }}" <?php if ($t == $tahun){ echo "selected";} ?>>{{ $t }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-md-1">

                                    </div>
                                    <div class="input-group col-md-1">
                                        <button type="submit" class="btn btn-success btn-sm">Submit</button>
                                    </div>
                                    <div class="input-group col-md-1">
                                        <a class="btn btn-danger btn-sm" role="button" href="/peramalan">Reset</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered">
                                <thead align="center">
                                <tr>
                                    <th>Periode</th>
                                    <th>Limbah Tani</th>
                                    <th>Alpha</th>
                                    <th>Nilai Peramalan</th>
                                    <th>MAPE</th>
                                    <th>Safety Stock</th>
                                </tr>
                                </thead>
                                <tbody align="center">
                                <tr>
                                    <td>{{ date('m-Y', strtotime($hasil[$indexMapeTerkecil][(count($periode)-1)]['periode'])) }}</td>

                                    <td>{{ $bb['nama_bahanbaku'] }}</td>

                                    <td>{{ number_format($hasil[$indexMapeTerkecil][(count($periode)-1)]['alpha'],1,'.','') }}</td>

                                    <td>{{ number_format($hasil[$indexMapeTerkecil][(count($periode)-1)]['hasil'],2,'.','') }}</td>

                                    <td>{{ number_format($hasil[$indexMapeTerkecil][(count($periode)-1)]['MAPE'],6,'.','').' %' }}</td>

                                    <td>{{ $ss }}</td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer">
                            <strong class="card-title">Kesimpulan</strong>
                            <br>
                            <a>1. Hasil rekomendasi Alpha peramalan {{ $bb['nama_bahanbaku'] }} pada bulan {{ $bln['nama'][$bulan-1] }} tahun {{ $tahun }} dengan perolehan MAPE terkecil ({{ number_format($hasil[$indexMapeTerkecil][(count($periode)-1)]['MAPE'],6,'.','').' %' }}) adalah {{ number_format($hasil[$indexMapeTerkecil][(count($periode)-1)]['alpha'],1,'.','') }}.</a>
                            <br>
                            <a>2. Hasil prediksi {{ $bb['nama_bahanbaku'] }} yang harus dipesan pada bulan {{ $bln['nama'][$bulan-1] }} tahun {{ $tahun }} adalah {{ number_format($hasil[$indexMapeTerkecil][(count($periode)-1)]['hasil'],0,'.','') }} Kg.</a>
                            <br>
                            <a>3. Hasil Safety Stock untuk {{ $bb['nama_bahanbaku'] }} pada bulan {{ $bln['nama'][$bulan-1] }} tahun {{ $tahun }} adalah {{ number_format($ss,0,'.','') }} Kg.</a>

                            <br>
                            <br>
                            <form action="/simpanperamalan" method="post" class="">
                                {{ csrf_field() }}
                                <input type="hidden" name="bahanbaku_id" value="{{ $bb['id_bahanbaku'] }}">
                                <input type="hidden" name="tanggal_peramalan" value="{{ date('Y-m-d') }}">
                                <input type="hidden" name="periode" value="{{ $hasil[$indexMapeTerkecil][(count($periode)-1)]['periode'] }}">
                                <input type="hidden" name="alpha" value="{{ number_format($hasil[$indexMapeTerkecil][(count($periode)-1)]['alpha'],1,'.','') }}">
                                <input type="hidden" name="aktual" value="{{ number_format($hasil[$indexMapeTerkecil][(count($periode)-1)]['aktual'],1,'.','') }}">
                                <input type="hidden" name="hasil_peramalan" value="{{ number_format($hasil[$indexMapeTerkecil][(count($periode)-1)]['hasil'],2,'.','') }}">
                                <input type="hidden" name="PE" value="{{ number_format($hasil[$indexMapeTerkecil][(count($periode)-1)]['PE'],2,'.','') }}">
                                <input type="hidden" name="MAPE" value="{{ number_format($hasil[$indexMapeTerkecil][(count($periode)-1)]['MAPE'],6,'.','') }}">
                                <input type="hidden" name="safety_stock" value="{{ $ss }}">
                                <button type="submit" class="btn btn-success btn-block">Simpan</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Detail Hasil Peramalan</strong>
                        </div>
                        <div class="card-body">
                            @for($i=0; $i < count($hasil); $i++)
                                <strong class="card-title">Alpha {{ $alpha[$i] }}</strong>
                                <table class="table table-bordered">
                                    <thead align="center">
                                    <tr>
                                        <th>Periode</th>
                                        <th>Aktual</th>
                                        <th>Hasil</th>
                                        <th>Alpha</th>
                                        <th>PE</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @for($j=0; $j<count($periode); $j++)
                                        <tr>
                                            <td>{{ date('m-Y', strtotime($hasil[$i][$j]['periode'])) }}</td>
                                            <td>{{ number_format($hasil[$i][$j]['aktual'],2,'.','') }}</td>
                                            <td>{{ number_format($hasil[$i][$j]['hasil'],2,'.','') }}</td>
                                            <td>{{ number_format($hasil[$i][$j]['alpha'],1,'.','') }}</td>
                                            <td>{{ number_format($hasil[$i][$j]['PE'],2,'.','').' %' }}</td>
                                        </tr>
                                    @endfor
                                    </tbody>
                                    <tfoot align="center">
                                    <tr>
                                        <th colspan="4">MAPE</th>
                                        <th colspan="1">{{ number_format($MAPE[$i],6,'.','').' %' }}</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            @endfor
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .animated -->
    </div><!-- .content -->
@endsection
@section('js')
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <script src="../vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../assets/js/main.js"></script>


    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <script src="../assets/js/init-scripts/data-table/datatables-init.js"></script>

@endsection
