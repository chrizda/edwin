@extends('admin/templateadmin')
@section('judul')
    Halaman - Edit Akun
@endsection

@section('css')
    <link rel="stylesheet" href="../vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="../vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="../vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="../vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">

    <link rel="stylesheet" href="../assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
@endsection

@section('content')

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-6 offset-md-3">
                                                    <div class="card">
                                                        <div class="card-header">Edit Akun</div>
                                                        <div class="card-body card-block">
                                                            <form action="/ubahAkun" method="post" enctype="multipart/form-data">
{{csrf_field()}}
                                                                <input type="hidden" name="id" value="{{$detail->id}}">
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                                                        <input type="text" id="name" name="name" placeholder="Nama Lengkap" class="form-control" value="{{$detail->name}}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                            <div class="row form-group">
                                                                    <div class="col col-md-4"><label for="file-input" class=" form-control-label">Masukkan Foto</label></div>
                                                                    <div class="float-right">
                                                                    <input type="file" id="foto" name="foto" class="form-control-file"></div>
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon"><i class="fa fa-phone"></i></div>
                                                                        <input type="text" id="no_hp" name="no_hp" placeholder="No. HP (aktif)" class="form-control" value="{{$detail->no_hp}}">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon"><i class="fa fa-home"></i></div>

                                <textarea name="alamat" placeholder="Masukkan Alamat sesuai KTP" rows="3" class="form-control t-text-area">{{$detail->alamat}}</textarea>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                                                        <input type="date" id="tgl_lahir" name="tgl_lahir" placeholder="Tanggal Lahir" class="form-control"  value="{{$detail->tgl_lahir}}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                                                        <input type="text" readonly id="username" name="username" placeholder="username" class="form-control" value="{{$detail->username}}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <div class="input-group">
                                                                        <div class="input-group-addon"><i class="fa fa-key"></i></div>
                                                                        <input type="password" id="password" name="password" placeholder="Password" value="{{$detail->password}}" class="form-control">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                             
                                                             <div class="modal-footer">
                                                                <a class="btn btn-primary btn-sm" role="button" href="/pengaturanAkun">Kembali</a>
                                                                <button type="submit" class="btn btn-success btn-sm">Submit</button>
                                                            </div>

                                                            </form>
                                                        </div>
                                                    </div>

                    </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->

@endsection
@section('js')



    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <script src="../vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../assets/js/main.js"></script>


    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <script src="../assets/js/init-scripts/data-table/datatables-init.js"></script>
@endsection

