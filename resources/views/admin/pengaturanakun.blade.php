@extends('admin/templateadmin')
@section('judul')
    Halaman - Manajemen Akun
@endsection
@section('css')
    <link rel="stylesheet" href="../vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="../vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="../vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="../vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">

    <link rel="stylesheet" href="../assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
@endsection

@section('content')
        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Data Mitra Tani</strong>
                                <div class="float-right">
                                    <a class="btn btn-info btn-sm" href="/akunBaru" role="button">Tambah Data</a>
                                
                                    </div>
                            </div>
                            <div class="card-body">
                                <table id="bootstrap-data-table-export" class="table table-bordered">
                                    @if(\Session::has('alert'))
                                        <div class="alert alert-success text-center">
                                            <div>{{Session::get('alert')}}</div>
                                        </div>
                                    @endif
                                        @if(\Session::has('error'))
                                            <div class="alert alert-danger text-center">
                                                <div>{{Session::get('error')}}</div>
                                            </div>
                                        @endif
                                        @if(\Session::has('ubah'))
                                            <div class="alert alert-primary text-center">
                                                <div>{{Session::get('ubah')}}</div>
                                            </div>
                                        @endif
                                    <thead align="center">
                                        <tr>
                                            <th width="10px">#</th>
                                            <th width="300px">Biodata</th>
                                            <th width="85px">Foto</th>
                                            
                                            
                                            <th width="30px">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @php $no=1;
                                    @endphp
                                    @foreach($users as $user)

                                        <tr>
                                            <th>{{$no}}</th>
                                            <td>

                                                <div class="col-12">
                                                    <ol>

                                                        <li >
                                                            Nama : {{$user->name}}
                                                        </li>
                                                        <hr>
                                                        <li >
                                                            Tgl. Lahir : {{date_format($user->tgl_lahir,'d M Y')}}
                                                        </li>
                                                        <hr>
                                                        <li >
                                                            No Hp : {{$user->no_hp}}
                                                        </li>
                                                        <hr>
                                                        <li >
                                                            Alamat : {{$user->alamat}}
                                                        </li>


                                                    </ol>
                                                </div>

                                            </td>
                                            <td align="center">
                                                <img class="rounded-circle mr-3" style="width:100px; height:100px;" alt="" src="../images/{{$user->foto}}">
                                            </td>


                                            <td>
                                                <br>
                                                <div class="card-body">
                                                    <a type="button" class="btn btn-primary btn-block" href="{{route('ubahakun',$user->id)}}">Ubah</a>
                                                    <a type="button" class="btn btn-danger mb-1 btn-block" href="{{route('deleteakun',$user->id)}}" onclick="return confirm('Apakah anda yakin menghapus user ini?')">Hapus</a>

                                                </div>
                                            </td>
                                        </tr>
                                        @php $no++ @endphp
                                    @endforeach
                                        {{--<tr>--}}
                                            {{--<th>1</th>--}}
                                            {{--<td>--}}
                                                {{----}}
                                                {{--<div class="col-12">--}}
                                                {{--<ol>--}}

                                                {{--<li >--}}
                                                    {{--Nama : Agus Supriadi--}}
                                                {{--</li>--}}
                                                {{--<hr>--}}
                                                {{--<li >--}}
                                                    {{--Tgl. Lahir : 25/10/1995--}}
                                                {{--</li>--}}
                                                {{--<hr>--}}
                                                {{--<li >--}}
                                                    {{--No Hp : 081456781569--}}
                                                {{--</li>--}}
                                                {{--<hr>--}}
                                                {{--<li >--}}
                                                    {{--Alamat : Jalan Mastrip No. 2 RT. 002 RW. 011 Desa Sumberwaru, Kec. Banyuputih, Kab. Situbondo--}}
                                                {{--</li>--}}

                                               {{----}}
                                                {{--</ol>--}}
                                                {{--</div>    --}}
                                            {{----}}
                                            {{--</td>--}}
                                            {{--<td align="center">--}}
                                            {{--<img class="rounded-circle mr-3" style="width:100px; height:100px;" alt="" src="../images/mitratani1.jpg">    --}}
                                            {{--</td>--}}
                                            {{----}}
                                            {{----}}
                                            {{--<td>--}}
                                                {{--<br>--}}
                                            {{--<div class="card-body">    --}}
                                              {{--<button type="button" class="btn btn-primary btn-block">Ubah</button>--}}
                                              {{--<button type="button" class="btn btn-danger mb-1 btn-block" data-toggle="modal" data-target="#hapusakun">Hapus</button>--}}
                                              {{----}}
                                            {{--</div>  --}}
                                            {{--</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<th>1</th>--}}
                                            {{--<td>--}}
                                                {{----}}
                                                {{--<div class="col-12">--}}
                                                {{--<ol>--}}

                                                {{--<li >--}}
                                                    {{--Nama : Agus Supriadi--}}
                                                {{--</li>--}}
                                                {{--<hr>--}}
                                                {{--<li >--}}
                                                    {{--Tgl. Lahir : 25/10/1995--}}
                                                {{--</li>--}}
                                                {{--<hr>--}}
                                                {{--<li >--}}
                                                    {{--No Hp : 081456781569--}}
                                                {{--</li>--}}
                                                {{--<hr>--}}
                                                {{--<li >--}}
                                                    {{--Alamat : Jalan Mastrip No. 2 RT. 002 RW. 011 Desa Sumberwaru, Kec. Banyuputih, Kab. Situbondo--}}
                                                {{--</li>--}}
                                               {{----}}
                                                {{--</ol>--}}
                                                {{--</div>    --}}
                                            {{----}}
                                            {{--</td>--}}
                                            {{--<td align="center">--}}
                                            {{--<img class="rounded-circle mr-3" style="width:100px; height:100px;" alt="" src="../images/mitratani2.jpg">    --}}
                                            {{--</td>--}}
                                            {{----}}
                                           {{----}}
                                            {{--<td>--}}
                                                {{--<br>--}}
                                            {{--<div class="card-body">    --}}
                                              {{--<button type="button" class="btn btn-primary btn-block">Ubah</button>--}}
                                              {{--<button type="button" class="btn btn-danger btn-block">Hapus </button>--}}
                                            {{--</div>  --}}
                                            {{--</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<th>1</th>--}}
                                            {{--<td>--}}
                                                {{----}}
                                                {{--<div class="col-12">--}}
                                                {{--<ol>--}}

                                                {{--<li >--}}
                                                    {{--Nama : Agus Supriadi--}}
                                                {{--</li>--}}
                                                {{--<hr>--}}
                                                {{--<li >--}}
                                                    {{--Tgl. Lahir : 25/10/1995--}}
                                                {{--</li>--}}
                                                {{--<hr>--}}
                                                {{--<li >--}}
                                                    {{--No Hp : 081456781569--}}
                                                {{--</li>--}}
                                                {{--<hr>--}}
                                                {{--<li >--}}
                                                    {{--Alamat : Jalan Mastrip No. 2 RT. 002 RW. 011 Desa Sumberwaru, Kec. Banyuputih, Kab. Situbondo--}}
                                                {{--</li>--}}
                                               {{----}}
                                                {{--</ol>--}}
                                                {{--</div>    --}}
                                            {{----}}
                                            {{--</td>--}}
                                            {{--<td align="center">--}}
                                            {{--<img class="rounded-circle mr-3" style="width:100px; height:100px;" alt="" src="../images/mitratani3.jpg">    --}}
                                            {{--</td>--}}
                                            {{----}}
                                            {{----}}
                                            {{--<td>--}}
                                                {{--<br>--}}
                                            {{--<div class="card-body">    --}}
                                              {{--<button type="button" class="btn btn-primary btn-block">Ubah</button>--}}
                                              {{--<button type="button" class="btn btn-danger btn-block">Hapus </button>--}}
                                            {{--</div>  --}}
                                            {{--</td>--}}
                                        {{--</tr>--}}
                                        {{--<tr>--}}
                                            {{--<th>1</th>--}}
                                            {{--<td>--}}
                                                {{----}}
                                                {{--<div class="col-12">--}}
                                                {{--<ol>--}}

                                                {{--<li >--}}
                                                    {{--Nama : Agus Supriadi--}}
                                                {{--</li>--}}
                                                {{--<hr>--}}
                                                {{--<li >--}}
                                                    {{--Tgl. Lahir : 25/10/1995--}}
                                                {{--</li>--}}
                                                {{--<hr>--}}
                                                {{--<li >--}}
                                                    {{--No Hp : 081456781569--}}
                                                {{--</li>--}}
                                                {{--<hr>--}}
                                                {{--<li >--}}
                                                    {{--Alamat : Jalan Mastrip No. 2 RT. 002 RW. 011 Desa Sumberwaru, Kec. Banyuputih, Kab. Situbondo--}}
                                                {{--</li>--}}
                                               {{----}}
                                                {{--</ol>--}}
                                                {{--</div>    --}}
                                            {{----}}
                                            {{--</td>--}}
                                            {{--<td align="center">--}}
                                            {{--<img class="rounded-circle mr-3" style="width:100px; height:100px;" alt="" src="../images/mitratani4.jpg">    --}}
                                            {{--</td>--}}
                                            {{----}}
                                            {{----}}
                                            {{--<td>--}}
                                                {{--<br>--}}
                                            {{--<div class="card-body">    --}}
                                              {{--<button type="button" class="btn btn-primary btn-block">Ubah</button>--}}
                                              {{--<button type="button" class="btn btn-danger btn-block">Hapus </button>--}}
                                            {{--</div>  --}}
                                            {{--</td>--}}
                                        {{--</tr>--}}
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->


@endsection
@section('js')
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <script src="../vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../assets/js/main.js"></script>


    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <script src="../assets/js/init-scripts/data-table/datatables-init.js"></script>
@endsection
