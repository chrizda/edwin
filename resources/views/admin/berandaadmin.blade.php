
@extends('admin/templateadmin')

@section('judul')
    Beranda
@endsection
@section('css')
<link rel="stylesheet" href="../vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="../vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="../vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="../vendors/jqvmap/dist/jqvmap.min.css">


    <link rel="stylesheet" href="../assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>
@endsection


@section('content')
        <div class="content mt-3">

            @if($jumlahpengiriman>0)
                <div class="col-sm-12">
                    <a href="/pengiriman">
                    <div class="alert  alert-success fade show" role="alert">
                        Ada kiriman baru, segera cek halaman Pengiriman Limbah Tani
                    </div>
                    </a>
                </div>
            @endif
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Grafik - Data Aktual & Peramalan</strong>
                        </div>
                        <div class="card-body">
                            <div class="col-11">
                                <div class="col-3"></div>
                                <div class="col-6 text-center">
                                    <select id="bahanbaku" class="form-control">
                                        @foreach($bahanbaku as $bb)
                                            <option value="{{$bb->id_bahanbaku}}">{{$bb->nama_bahanbaku}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-11" id="chartLine">
                                <canvas id="lineChart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <strong class="card-title">Grafik - Total Stok</strong>
                        </div>
                        <div class="card-body">
                            <div class="col-11">
                                <div class="col-3"></div>
                                <div class="col-6 text-center">
                                    <select id="mitra" class="form-control">
                                        @foreach($mitra as $m)
                                            <option value="{{$m->user_id}}">{{$m->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-11" id="chartPie">
                                <canvas id="pieChart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>


        @endsection

    <!-- Right Panel -->
@section('js')
    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <script src="../vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../assets/js/main.js"></script>


    <script src="../vendors/chart.js/dist/Chart.bundle.min.js"></script>
    <script src="../assets/js/dashboard.js"></script>
    <script src="../assets/js/widgets.js"></script>
    <script src="../vendors/jqvmap/dist/jquery.vmap.min.js"></script>
    <script src="../vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <script src="../vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script>
        jQuery(document).ready(function($) {
            "use strict";

            // jQuery('#vmap').vectorMap({
            //     map: 'world_en',
            //     backgroundColor: null,
            //     color: '#ffffff',
            //     hoverOpacity: 0.7,
            //     selectedColor: '#1de9b6',
            //     enableZoom: true,
            //     showTooltip: true,
            //     values: sample_data,
            //     scaleColors: ['#1de9b6', '#03a9f5'],
            //     normalizeFunction: 'polynomial'
            // });


            var idbb = $('#bahanbaku').val();
            $.ajax({
                url:'/getGrafikBeranda/'+idbb,
                type:'GET',
                dataType:'json',
                success: function(data){
                    $('#lineChart').remove();
                    $('#chartLine').append('<canvas id="lineChart"></canvas>');
                    //line chart
                    let ctx = document.getElementById( "lineChart" );
                    ctx.height = 150;
                    var aktual = data.aktual;
                    var peramalan = data.peramalan;
                    var label = data.bln;
                    var myChart = new Chart( ctx, {
                        type: 'line',
                        data: {
                            labels: label,
                            datasets: [
                                {
                                    label: "Data Aktual",
                                    borderColor: "rgba(0,0,0,.09)",
                                    borderWidth: "1",
                                    backgroundColor: "rgba(0,0,0,.07)",
                                    data: aktual
                                },
                                {
                                    label: "Hasil Peramalan",
                                    borderColor: "rgba(0, 123, 255, 0.9)",
                                    borderWidth: "1",
                                    backgroundColor: "rgba(0, 123, 255, 0.5)",
                                    pointHighlightStroke: "rgba(26,179,148,1)",
                                    data: peramalan
                                }
                            ]
                        },
                        options: {
                            responsive: true,
                            tooltips: {
                                mode: 'index',
                                intersect: false
                            },
                            hover: {
                                mode: 'nearest',
                                intersect: true
                            }

                        }
                    } );
                }
            });

            $('#bahanbaku').on('change',function(){
                var idbb = $('#bahanbaku').val();
                $.ajax({
                    url:'/getGrafikBeranda/'+idbb,
                    type:'GET',
                    dataType:'json',
                    success: function(data){
                        $('#lineChart').remove();
                        $('#chartLine').append('<canvas id="lineChart"></canvas>');
                        //line chart
                        let ctx = document.getElementById( "lineChart" );
                        ctx.height = 150;
                        var aktual = data.aktual;
                        var peramalan = data.peramalan;
                        var label = data.bln;
                        var myChart = new Chart( ctx, {
                            type: 'line',
                            data: {
                                labels: label,
                                datasets: [
                                    {
                                        label: "Data Aktual",
                                        borderColor: "rgba(0,0,0,.09)",
                                        borderWidth: "1",
                                        backgroundColor: "rgba(0,0,0,.07)",
                                        data: aktual
                                    },
                                    {
                                        label: "Hasil Peramalan",
                                        borderColor: "rgba(0, 123, 255, 0.9)",
                                        borderWidth: "1",
                                        backgroundColor: "rgba(0, 123, 255, 0.5)",
                                        pointHighlightStroke: "rgba(26,179,148,1)",
                                        data: peramalan
                                    }
                                ]
                            },
                            options: {
                                responsive: true,
                                tooltips: {
                                    mode: 'index',
                                    intersect: false
                                },
                                hover: {
                                    mode: 'nearest',
                                    intersect: true
                                }

                            }
                        } );
                    }
                });
            })

        });

        jQuery(document).ready(function($){
            function dynamicColors() {
                var r = Math.floor(Math.random() * 255);
                var g = Math.floor(Math.random() * 255);
                var b = Math.floor(Math.random() * 255);
                return "rgba(" + r + "," + g + "," + b + ", 0.5)";
            }

            function poolColors(a) {
                var pool = [];
                for(var i = 0; i < a; i++) {
                    pool.push(dynamicColors());
                }
                return pool;
            }

            var id = $('#mitra').val();
            $.ajax({
                url:'/getStokBeranda/'+id,
                type:'GET',
                dataType:'json',
                success: function(data){
                    $('#pieChart').remove();
                    $('#chartPie').append('<canvas id="pieChart"></canvas>');
                    let ct = document.getElementById( "pieChart" );
                    var arr = [];
                    var label = [];
                    data.forEach(function(datum){
                        arr.push(datum.jumlah);
                        label.push(datum.nama_bahanbaku);
                    });
                    var chart = new Chart( ct, {
                        type: 'pie',
                        data: {
                            datasets: [ {
                                data: arr,
                                backgroundColor: poolColors(arr.length),
                            } ],
                            labels: label
                        },
                        options: {
                            responsive: true
                        }
                    } );
                }
            })

            $('#mitra').on('change',function(){
                var id = $('#mitra').val();
                $.ajax({
                    url:'/getStokBeranda/'+id,
                    type:'GET',
                    dataType:'json',
                    success: function(data){
                        $('#pieChart').remove();
                        $('#chartPie').append('<canvas id="pieChart"></canvas>');
                        let ct = document.getElementById( "pieChart" );
                        var arr = [];
                        var label = [];
                        data.forEach(function(datum){
                            arr.push(datum.jumlah);
                            label.push(datum.nama_bahanbaku);
                        });
                        var chart = new Chart( ct, {
                            type: 'pie',
                            data: {
                                datasets: [ {
                                    data: arr,
                                    backgroundColor: poolColors(arr.length),
                                } ],
                                labels: label
                            },
                            options: {
                                responsive: true
                            }
                        } );
                    }
                })
            })
        });
    </script>
@endsection
{{--//line chart--}}
{{--var ctx = document.getElementById( "lineChart" );--}}
{{--ctx.height = 150;--}}
{{--var myChart = new Chart( ctx, {--}}
{{--type: 'line',--}}
{{--data: {--}}
{{--labels: [ @for($i=0; $i<count($bln['nama']); $i++) "{{ $bln['nama'][$i] }}", @endfor],--}}
{{--datasets: [--}}
{{--{--}}
{{--label: "Data Aktual",--}}
{{--borderColor: "rgba(0,0,0,.09)",--}}
{{--borderWidth: "1",--}}
{{--backgroundColor: "rgba(0,0,0,.07)",--}}
{{--data: [ @for($a=0; $a<count($aktual); $a++) {{ $aktual[$a] }}, @endfor ]--}}
{{--},--}}
{{--{--}}
{{--label: "Hasil Peramalan",--}}
{{--borderColor: "rgba(0, 123, 255, 0.9)",--}}
{{--borderWidth: "1",--}}
{{--backgroundColor: "rgba(0, 123, 255, 0.5)",--}}
{{--pointHighlightStroke: "rgba(26,179,148,1)",--}}
{{--data: [ @for($p=0; $p<count($peramalan); $p++) {{ $peramalan[$p] }}, @endfor ]--}}
{{--}--}}
{{--]--}}
{{--},--}}
{{--options: {--}}
{{--responsive: true,--}}
{{--tooltips: {--}}
{{--mode: 'index',--}}
{{--intersect: false--}}
{{--},--}}
{{--hover: {--}}
{{--mode: 'nearest',--}}
{{--intersect: true--}}
{{--}--}}

{{--}--}}
{{--} );--}}
