@extends('admin/templateadmin')
@section('judul')
    Halaman - Pengiriman Bahan Baku
@endsection

@section('css')

    <link rel="stylesheet" href="../vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="../vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="../vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="../vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="../vendors/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css">

    <link rel="stylesheet" href="../assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>

@endsection

    @section('content')

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Data Tunggu - Pengiriman Bahan Baku</strong>
                            </div>
                            <div class="card-body">
                                @if(\Session::has('alert'))
                                    <div class="alert alert-success text-center">
                                        <div>{{Session::get('alert')}}</div>
                                    </div>
                                @endif
                                @if(\Session::has('error'))
                                    <div class="alert alert-danger text-center">
                                        <div>{{Session::get('error')}}</div>
                                    </div>
                                @endif
                                <table id="kiriman" class="table table-bordered">
                                    <thead align="center">
                                        <tr>
                                            <th>#</th>
                                            <th width="230px">Bahan Baku</th>
                                            <th width="20px">Tanggal</th>
                                            <th>Biodata : Mitra Tani</th>
                                            <th width="100px">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @php if(!empty($a)){$b = $a->getContent(); $c = json_decode($b);}else{$c="Kosong";} $no=1; @endphp
                                    @if($c!="Kosong")
                                        @foreach($c as $trans)
                                            <tr>
                                                <th class="text-center" style="vertical-align: middle">{{$no}}</th>
                                                <td style="vertical-align: middle">
                                                    <div class="col-12">
                                                        <ol>
                                                            @foreach($trans->trans as $detail)
                                                                <li>
                                                                    {{$detail->nama_bahanbaku}} <span class="badge badge-secondary pull-right">{{$detail->jumlah}} {{$detail->satuan}}</span>
                                                                </li>
                                                                @if(!$loop->last)
                                                                    @if(count($trans->trans)!=1)
                                                                        <hr>
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        </ol>
                                                    </div>
                                                </td>
                                                <td class="text-center" style="vertical-align: middle;">
                                                    <h5>@php echo date('d/M/Y',strtotime($trans->tanggal));@endphp</h5>
                                                </td>
                                                <td>
                                                    <div class="col-12">
                                                        <ol>
                                                                <li>
                                                                    Nama: {{$trans->user->name}}
                                                                </li>
                                                            <hr>
                                                                <li>
                                                                    No. Hp: {{$trans->user->no_hp}}
                                                                </li>
                                                            <hr>
                                                                <li>
                                                                    Alamat: {{$trans->user->alamat}}
                                                                </li>
                                                        </ol>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="card-body">
                                                        <a href="{{route('terima',$trans->id_transaksi)}}" type="button" class="btn btn-success btn-block">Terima</a>
                                                        <a href="{{route('tolak',$trans->id_transaksi)}}" type="button" class="btn btn-danger mb-1 btn-block" onclick="return confirm('Tolak transaksi ini?')">Tolak</a>
                                                    </div>
                                                </td>
                                            </tr>
                                            @php $no++;@endphp
                                        @endforeach
                                    @endif
{{--                                        <tr>--}}
{{--                                            <th>1</th>--}}
{{--                                            <td>--}}
{{--                                                <div class="col-12">--}}
{{--                                                    <ol>--}}
{{--                                                        <li>--}}
{{--                                                             Limbah Padi <span class="badge badge-secondary pull-right">100 Kg</span>--}}
{{--                                                         --}}
{{--                                                        </li>--}}
{{--                                                        <li>--}}
{{--                                                            <hr>--}}
{{--                                                             Limbah Jagung <span class="badge badge-secondary pull-right">150 Kg</span>--}}
{{--                                                         --}}
{{--                                                        </li>--}}
{{--                                                        <li>--}}
{{--                                                            <hr>--}}
{{--                                                            Limbah Tebu<span class="badge badge-secondary pull-right">110 Kg</span>--}}
{{--                                                            --}}
{{--                                                        </li>--}}
{{--                                                    </ol>--}}
{{--                                                </div>--}}
{{--                                            </td>--}}
{{--                                            <td align="center"> --}}
{{--                                            <br>--}}
{{--                                            <br>--}}
{{--                                            <br>--}}
{{--                                                <h5>12/02/19</h5> --}}
{{--                                            </td>--}}
{{--                                            <td>--}}
{{--                                                --}}
{{--                                                <div class="col-12">--}}
{{--                                                <ol>--}}

{{--                                                <li >--}}
{{--                                                    Nama : Agus Supriadi--}}
{{--                                                </li>--}}
{{--                                                <hr>--}}
{{--                                                <li >--}}
{{--                                                    No Hp : 081456781569--}}
{{--                                                </li>--}}
{{--                                                <hr>--}}
{{--                                                <li >--}}
{{--                                                    Alamat : Jalan Mastrip No. 2 RT. 002 RW. 011 Desa Sumberwaru, Kec. Banyuputih, Kab. Situbondo--}}
{{--                                                </li>--}}
{{--                                               --}}
{{--                                                </ol>--}}
{{--                                                </div>    --}}
{{--                                            --}}
{{--                                            </td>--}}
{{--                                            <td>--}}
{{--                                                <br>--}}
{{--                                            <div class="card-body">    --}}
{{--                                              <button type="button" class="btn btn-success btn-block">Terima</button>--}}
{{--                                              <button type="button" class="btn btn-danger mb-1 btn-block" data-toggle="modal" data-target="#tolakkirim">Tolak</button>--}}
{{--                                            </div>  --}}
{{--                                            </td>--}}
{{--                                        </tr>--}}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>


                </div>
            </div><!-- .animated -->
        </div><!-- .content -->


    <!-- Right Panel -->
@endsection
@section('js')

    <script src="../vendors/jquery/dist/jquery.min.js"></script>
    <script src="../vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="../vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="../assets/js/main.js"></script>



    <script src="../vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="../vendors/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
    <script src="../vendors/jszip/dist/jszip.min.js"></script>
    <script src="../vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="../vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="../vendors/datatables.net-buttons/js/buttons.colVis.min.js"></script>
    <script src="../assets/js/init-scripts/data-table/datatables-init.js"></script>

    <script>
        jQuery('#kiriman').dataTable({
           "language":{
               "emptyTable": "Tidak ada kiriman"
           }
        });
    </script>
@endsection

