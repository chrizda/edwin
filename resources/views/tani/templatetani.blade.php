<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sistem Bahan Baku Ternak Sapi</title>
    <meta name="description" content="Sufee Admin - HTML5 Admin Template">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="../apple-icon.png">
    <link rel="shortcut icon" href="../favicon.ico">

    @yield('css')
</head>

<body>
<aside id="left-panel" class="left-panel">
    <nav class="navbar navbar-expand-sm navbar-default">

        <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <br>
            <div class="media">

                <img class="align-self-center rounded-circle mr-3" style="width:85px; height:85px;" alt="" src="../images/{{Session::get('foto')}}">

                <div class="media-body">
                    <h2 class="text-light display-6">{{Session::get('name')}}</h2>
                    <p class="text-light display-6">Mitra Tani</p>
                </div>
            </div>

        </div>

        <h3 class="menu-title">Menu - Mitra Tani</h3>

        <div id="main-menu" class="main-menu collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active">
                    <a href="/gudangTani"> <i class="menu-icon fa fa-truck"></i>Stok Limbah Tani </a>
                </li>
                <li class="active">
                    <a href="/riwayatPengiriman"> <i class="menu-icon fa fa-star"></i>Riwayat Pengiriman </a>
                </li>


            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
</aside><!-- /#left-panel -->

<!-- Left Panel -->

<!-- Right Panel -->

<div id="right-panel" class="right-panel">

    <div class="breadcrumbs">
        <div class="col-sm-4">
            <div class="page-header float-left">
                <div class="page-title">
                    <h1>@yield('judul')</h1>
                </div>
            </div>
        </div>
        <div class="col-sm-8">
            <div class="float-right">

                <a class="nav-link" href="/logout"><i class="fa fa-power-off"></i> Keluar</a>

            </div>
        </div>
    </div>

    @yield('content')
</div>

@yield('js')
</body>
</html>
