<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Peramalan extends Model  {

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'peramalan';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id_peramalan', 'bahanbaku_id', 'tanggal_peramalan', 'periode', 'alpha', 'aktual', 'hasil_peramalan', 'PE', 'MAPE', 'safety_stock'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['tanggal_peramalan'];

    public $timestamps=false;

    public static function deletePeramalan($id){
        $del = DB::table('peramalan')->where('id_peramalan',$id)
            ->delete();
        return $del;
    }

    public static function getPeramalanBeranda($idbahanbaku){
        $now = date('Y');

        $data = Peramalan::selectRaw('periode, hasil_peramalan')
            ->where('bahanbaku_id', $idbahanbaku)
            ->get();
        //ini ambil periodenya
        $periode= array();
        $month = date('Y-m', strtotime($now.'-01'));
        $i = 0;
        while(date('Y-m', strtotime($month)) <= date('Y-m', strtotime($now.'-12'))) {
            $periode[$i] = $month;
            $month = date('Y-m', strtotime("+1 month", strtotime(date($month))));
            $i++;
        }

        //ini menghitung total
        $peramalan = array();
        for($i=0; $i<count($periode); $i++) {
            for($j=0; $j<count($data); $j++) {
                if($periode[$i] == $data[$j]['periode']){
                    $peramalan[$i] = intval($data[$j]['hasil_peramalan']);
                    break;
                }else{
                    $peramalan[$i] = 0;
                }
            }
        }

        return $peramalan;
    }
}
