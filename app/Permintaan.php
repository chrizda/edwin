<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Permintaan extends Model
{



    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permintaan';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['bahanbaku_id', 'jumlah'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    public $timestamps=false;

    public static function getAllPermintaan(){
        $permintaan = DB::table('permintaan')->join('bahanbaku','bahanbaku_id','=','id_bahanbaku')->where('jumlah','>',0)->get();
        return $permintaan;
    }
}
