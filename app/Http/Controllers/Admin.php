<?php

namespace App\Http\Controllers;

use App\Permintaan;
use App\Setting;
use App\Transaksi;
use Illuminate\Http\Request;
use App\Users;
use App\Gudang;
use App\Bahanbaku;
use App\Stok;
use App\DetailTransaksi;
use App\Peramalan;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class Admin extends Controller
{
    //Fitur home
    public function __construct(){
        $this->middleware(function($request, $next){
            if(Session::get("status")!=1){
                return redirect()->back();
            }
            return $next($request);
        });
    }


    public function homeAdmin(){
        $jumlahpengiriman = Transaksi::getJumlahPengiriman();
        $mitra = Gudang::getAllMitra();
        $bahanbaku = Bahanbaku::all();
        $bln = array('nomer' => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], 'nama' => ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']);
        $aktual = Transaksi::getAktualBeranda(1);
        $peramalan = Peramalan::getPeramalanBeranda(1);

        return view('admin/berandaadmin',compact('jumlahpengiriman','mitra', 'bahanbaku', 'bln', 'aktual', 'peramalan'));
    }

    public function getStokBeranda($id){
        $stok = Stok::getAllStokTaniByUser($id)->toJson();
        return $stok;
    }

    public function getGrafikBeranda($id){
        $aktual = Transaksi::getAktualBeranda($id);
        $peramalan = Peramalan::getPeramalanBeranda($id);
        $bln = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');

        $data['aktual'] = $aktual;
        $data['peramalan'] = $peramalan;
        $data['bln'] = $bln;

        $a='';
        if(isset($data)) {
            $a = response()->json($data);
        }

        return $a;
    }

    public function getPeramalanBeranda($id){
        $peramalan = Peramalan::getPeramalanBeranda($id);
        return $peramalan;
    }

    //Fitur pengiriman

    public function pengiriman(){
        $jumlah = Transaksi::jumlahPengirimanBahanBaku();
        $allid = Transaksi::getAllIdPengiriman();

        for ($i=1;$i<$jumlah+1;$i++){
            $jumlahdetail = Transaksi::getJumlahDetailTransaksi($allid[$i-1]->id_transaksi);
            for ($j=0;$j<$jumlahdetail;$j++){
                $data["trans".$allid[$i-1]->id_transaksi] = array(
                    "id_transaksi"=>$allid[$i-1]->id_transaksi,
                    "tanggal"=> $allid[$i-1]->tanggal_transaksi,
                    "trans"=> Transaksi::getDetailById($allid[$i-1]->id_transaksi),
                    "user"=> Transaksi::getDetailUser($allid[$i-1]->gudang_id)
                );
            }
        }
        $a='';
        if(isset($data)) {
            $a = response()->json($data);
        }

        return view('admin/pengirimanbahanbaku',compact('a'));
    }

    public function terima($idtrans){
        DB::table('transaksi')->where('id_transaksi','=',$idtrans)
            ->update(['status'=>2]);

        $detail = Transaksi::getDetailById($idtrans);
        foreach($detail as $barang){
            Transaksi::tambahStokSetelahPengiriman($barang->bahanbaku_id,$barang->jumlah);
            $permintaan = Permintaan::where('bahanbaku_id',$barang->bahanbaku_id)->first();
            if(!$permintaan){
            }else {
                if ($permintaan->jumlah - $barang->jumlah < 0) {
                    $permintaan->delete();
                } else {
                    $permintaan->jumlah = ($permintaan->jumlah - $barang->jumlah);
                    $permintaan->save();
                }
            }
        }



        return redirect('/pengiriman')->with('alert','Pengiriman berhasil diterima');
    }

    public function tolak($idtrans){
        DB::table('transaksi')->where('id_transaksi','=',$idtrans)
            ->update(['status'=>3]);

        $trans = Transaksi::getTransDetailById($idtrans); //tambahan buat kembalikan stok ke mitra tani
        foreach ($trans as $t){
            DB::table('stok')->where('bahanbaku_id',$t->bahanbaku_id)
                ->where('gudang_id','=',$t->gudang_id)
                ->update([
                    'jumlah'=> DB::raw('jumlah+'.$t->jumlah)
                ]);
        }
        return redirect('/pengiriman')->with('error','Pengiriman berhasil ditolak');
    }

    //Fitur gudang

    public function gudang(){
        $dataStok = Stok::stokBahanBakuAdmin();
        $dataStatus = Setting::getSetting();
        return view('admin/gudangbahanbakuedwin',compact('dataStok','dataStatus'));
    }

    public function tambahStok(Request $request){
        DB::table('stok')->where('bahanbaku_id',$request->idbb)
            ->where('gudang_id','=',1)
            ->update([
               'jumlah'=> DB::raw('jumlah+'.$request->tambahan)
            ]);
        return redirect('/gudangAdmin');
    }

    public function tambahBB(Request $request){
        DB::beginTransaction();
        $namabahanbaku = Str::title($request->namabb);
        $arraynama = explode(" ",$namabahanbaku);
        if($arraynama[0]!="Limbah"){
            return redirect ('/gudangAdmin')->with('error', 'Hanya dapat menambahkan data limbah');
        }
        $bb = Bahanbaku::firstOrCreate(['nama_bahanbaku'=>$namabahanbaku]);

        if($bb->wasRecentlyCreated == false){
            $idnya = $bb->id_bahanbaku;
            $stok = new Stok();
            $stok->gudang_id = 1;
            $stok->bahanbaku_id = $idnya;
            $stok->jumlah = $request->stok;
            $stok->satuan = "Kg";
            $stok->save();

            $allidgudang = Gudang::getAllIdGudang();
            foreach($allidgudang as $idgudang){
                $stok = new Stok();
                $stok->gudang_id = $idgudang;
                $stok->bahanbaku_id = $idnya;
                $stok->jumlah = 0;
                $stok->satuan = "Kg";
                $stok->save();
            }

            if (!$bb || !$stok) {
                DB::rollBack();
                return redirect('/gudangAdmin')->with('error', 'Data bahan baku gagal ditambahkan');
            } else {
                DB::commit();
            }
        }else {
            $idbb = $bb->id;

            $stok = new Stok();
            $stok->gudang_id = 1;
            $stok->bahanbaku_id = $idbb;
            $stok->jumlah = $request->stok;
            $stok->satuan = "Kg";
            $stok->save();

            $allidgudang = Gudang::getAllIdGudang();
            foreach($allidgudang as $idgudang){
                $stok = new Stok();
                $stok->gudang_id = $idgudang;
                $stok->bahanbaku_id = $idbb;
                $stok->jumlah = 0;
                $stok->satuan = "Kg";
                $stok->save();
            }

            if (!$bb || !$stok) {
                DB::rollBack();
                return redirect('/gudangAdmin')->with('error', 'Data bahan baku gagal ditambahkan');
            } else {
                DB::commit();
            }
        }

        return redirect('/gudangAdmin')->with('alert','Data bahan baku berhasil ditambahkan');
    }

    public function hapusBB($idBB){
//        if($idBB=="1"||$idBB=="2"||$idBB=="3"||$idBB=="4"||$idBB=="5"){
//            return redirect('/gudangAdmin')->with('error', 'Tolong jangan usil menghapus yang tidak ada tombol hapusnya');
//        }else {
            $delete = Stok::deleteStok($idBB);
            if ($delete) {
                return redirect('/gudangAdmin')->with('error', 'Data bahan baku berhasil dihapus');
            } else {
                return redirect('/gudangAdmin')->with('error', 'Data bahan baku gagal dihapus');
            }
//        }

    }

    public function kirimBB(Request $request){
        if(count($request->all()) == 1){
            return redirect('/gudangAdmin')->with('error','Belum ada data limbah yang akan dikirim');
        }
        DB::beginTransaction();
        $trans = new Transaksi();
        $trans->tanggal_transaksi = date('Y-m-d');
        $trans->gudang_id = 1;
        $trans->status = 9; //status 9 itu admin ngirim ke produksi
        $trans->save();
        $idtrans = $trans->id;
        for($i=0;$i<count($request->idbb);$i++){
            $idnya = $request->idbb[$i];
            $qty = $request->qty[$i];
            $satuan = $request->sat[$i];

            $semuakolom = DB::table('stok')->where('bahanbaku_id',$idnya)->first();
            $qtydidb = $semuakolom->jumlah;
            if($qty > $qtydidb){
                DB::rollBack();
                return redirect('/gudangAdmin')->with('error', 'Data bahan baku gagal ditambahkan, mohon periksa kembali jumlah pemesanan');
            }else{
                $detail = new DetailTransaksi();
                $detail->bahanbaku_id = $idnya;
                $detail->jumlah = $qty;
                $detail->satuan = $satuan;
                $detail->transaksi_id = $idtrans;
                $detail->save();

                DB::table('stok')->where('bahanbaku_id',$idnya)->update(['jumlah'=>DB::raw('jumlah-'.$qty)]);
            }
        }
        DB::commit();
        return redirect('/gudangAdmin')->with('alert','Data bahan baku berhasil dikirim');
    }

    public function tambahPermintaan(Request $request){
        $jumlah = $request->permintaan;
        $id = $request->idbb;

        $permintaan = Permintaan::firstOrNew(['bahanbaku_id'=>$id]);
        $permintaan->jumlah = ($permintaan->jumlah + $jumlah);
        $permintaan->save();

        return redirect('/gudangAdmin')->with('alert','Data permintaan berhasil ditambahkan');
    }

    public function setNilai(Request $request){
        $nilai = $request->nilai;

        DB::table('setting')->where('id',1)->update(['jumlah'=>$nilai]);
        return redirect('/gudangAdmin')->with('alert','Data status berhasil diubah');
    }

    //Fitur riwayat

    public function riwayat(){
        $jumlah = Transaksi::jumlahRiwayatPengirimanAdmin();
        $allid = Transaksi::getAllIdTransaksi();

        for ($i=1;$i<$jumlah+1;$i++){
            $jumlahdetail = Transaksi::getJumlahDetailTransaksi($allid[$i-1]->id_transaksi);
            for ($j=0;$j<$jumlahdetail;$j++){
                $data["trans".$allid[$i-1]->id_transaksi] = array(
                    "tanggal"=> $allid[$i-1]->tanggal_transaksi,
                    "trans"=> Transaksi::getDetailById($allid[$i-1]->id_transaksi)
                );
            }
        }
        $a="";
        if(isset($data)) {
            $a = response()->json($data);
        }
        return view('admin/riwayatpengiriman',compact('a'));
    }

    //Fitur akun

    public function akun(){
        $users = Users::orderBy('id','ASC')->where('status','2')->where('is_deleted','0')->get();
        return view('admin/pengaturanakun', compact('users'));
    }

    public function akunbaru(){
        return view('admin/daftarakun');
    }

    public function postakun(Request $request){
        $this->validate($request,[
            'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'username' => 'required|unique:users,username',
            'password' => 'required',
            'name' => 'required',
            'tgl_lahir' => 'required',
            'no_hp' => 'required',
            'alamat' => 'required'
        ]);

        $imageName = time().'.'.request()->foto->getClientOriginalExtension();
        request()->foto->move(public_path('images'), $imageName);

        DB::beginTransaction();
        $user = new Users();
        $user->username = $request->username;
        $user->password = $request->password;
        $user->name = $request->name;
        $user->tgl_lahir = $request->tgl_lahir;
        $user->no_hp = $request->no_hp;
        $user->alamat = $request->alamat;
        $user->foto = $imageName;
        $user->status = 2;
        $user->is_deleted = 0;
        $user->save();

        $gudang = new Gudang();
        $gudang->user_id = $user->id;
        $gudang->save();

        $stok = new Stok();
        $stok->gudang_id = $gudang->id;
        $stok->bahanbaku_id = 1;
        $stok->jumlah = 0;
        $stok->satuan = "Kg";
        $stok->save();

        $stok = new Stok();
        $stok->gudang_id = $gudang->id;
        $stok->bahanbaku_id = 2;
        $stok->jumlah = 0;
        $stok->satuan = "Kg";
        $stok->save();

        $stok = new Stok();
        $stok->gudang_id = $gudang->id;
        $stok->bahanbaku_id = 3;
        $stok->jumlah = 0;
        $stok->satuan = "Kg";
        $stok->save();

        if(!$user || !$gudang){
            DB::rollBack();
            return redirect('/pengaturanAkun')->with('error','Data akun gagal ditambahkan, periksa kembali');
        }else{
            DB::commit();
        }
        return redirect('/pengaturanAkun')->with('alert','Data akun berhasil ditambahkan');
    }

    public function halamanubahakun($id){
        $detail = Users::getUser($id);
        return view('admin/ubahakun',compact('detail'));
    }

    public function ubahakun(Request $request){
        if($request->foto) {
            request()->validate([
                'foto' => 'image|mimes:jpeg,png,jpg,gif,svg',
            ]);
            $imageName = time() . '.' . request()->foto->getClientOriginalExtension();
            request()->foto->move(public_path('images'), $imageName);

            $id = $request->id;

            $data = array(
                "password" => $request->password,
                "name" => $request->name,
                "tgl_lahir" => $request->tgl_lahir,
                "no_hp" => $request->no_hp,
                "alamat" => $request->alamat,
                "foto" => $imageName
            );
        }else{
            $id = $request->id;

            $data = array(
                "password" => $request->password,
                "name" => $request->name,
                "tgl_lahir" => $request->tgl_lahir,
                "no_hp" => $request->no_hp,
                "alamat" => $request->alamat
            );
        }


        DB::table('users')->where('id',$id)
            ->update($data);

        return redirect('/pengaturanAkun')->with('ubah','Data akun berhasil diubah');
    }

    public function deleteakun($id){
        DB::table('users')->where('id',$id)
            ->update(["is_deleted"=>1]);
        return redirect('/pengaturanAkun')->with('error','Data akun berhasil dihapus');
    }


    //fitur peramalan

    public function peramalan(){
        $peramalan = Peramalan::join('bahanbaku', 'id_bahanbaku', '=', 'bahanbaku_id')->orderBy('bahanbaku_id','ASC')->orderBy('periode','ASC')->get();
        $bln = array('nomer' => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], 'nama' => ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']);
        $bahanbaku = Bahanbaku::all();
        return view('admin/peramalan', compact('peramalan', 'bln', 'bahanbaku'));
    }

    public function buatperamalan(request $request){
        $bln = array('nomer' => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12], 'nama' => ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember']);
        $peramalan = Peramalan::join('bahanbaku', 'id_bahanbaku', '=', 'bahanbaku_id')->orderBy('bahanbaku_id','ASC')->orderBy('periode','ASC')->get();
        $bahanbaku = Bahanbaku::all();

        $cek = Transaksi::all();
        if (count($cek) < 1){
            return redirect('/peramalan')->with('error','Belum ada data transaksi, tidak bisa meramal.');
        }else{
            $id_bahanbaku = $request->bahanbaku;
            $bb = Bahanbaku::where('id_bahanbaku', $id_bahanbaku)->first();
            $bulan = $request->bulan;
            $tahun = $request->tahun;
            $tanggal = $tahun.'-'.$bulan;

            //ambil tanggal paling awal
            $from = Transaksi::orderBy('tanggal_transaksi', 'ASC')->first()->tanggal_transaksi;

            //ini ambil data transaksi dari db
            $data = Transaksi::selectRaw('DATE_FORMAT(tanggal_transaksi, "%Y-%m") as periode, sum(jumlah) as total')
                ->join('detail_transaksi', 'transaksi.id_transaksi', '=', 'detail_transaksi.transaksi_id')
                ->where('bahanbaku_id', $id_bahanbaku)
                ->whereRaw("DATE_FORMAT(tanggal_transaksi, '%Y-%m') <= '$tanggal'")
                ->groupBy('periode')
                ->get();

            //ini ambil periodenya
            $periode= array();
            $month = date('Y-m', strtotime($from));
            $i = 0;
            while(date('Y-m', strtotime($month)) <= date('Y-m', strtotime($tanggal))) {
                $periode[$i] = $month;
                $month = date('Y-m', strtotime("+1 month", strtotime(date($month))));
                $i++;
            }

            //ini menghitung total
            $X = array();
            for($i=0; $i<count($periode); $i++) {
                for($j=0; $j<count($data); $j++) {
                    if($periode[$i] == $data[$j]['periode']){
                        $X[$i] = intval($data[$j]['total']);
                        break;
                    }else{
                        $X[$i] = 0;
                    }
                }
            }

            //perhitungan SES
            $F = array();
            $alpha = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9];
            $alpha2= [0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1];
            $PE = array();
            $MAPE = array();

            // perhitungan peramalan menggunakan nilai alpha mulai dari 0.1 sampai 0.9
            for($i = 0; $i < count($alpha); $i++) {
                // inisialisasi
                $F[$i][0] = $PE[$i][0] = 0;

                for($j = 1; $j < count($periode); $j++){
                    // perhitungan peramalan untuk periode berikutnya
                    $F[$i][$j] = ($alpha[$i] * $X[$j-1]) + ($alpha2[$i] * $F[$i][$j-1]);

                    // menghitung nilai kesalahan persentase peramalan
                    $PE[$i][$j] = $X[$j] == 0 ? 0 : abs((($X[$j] - $F[$i][$j]) / $X[$j]) * 100);
                }
                // menghitung rata-rata kesalahan peramalan
                if (count($periode) > 1){
                    $MAPE[$i] = array_sum($PE[$i])/(count($periode) - 1);
                }else{
                    $MAPE[$i] = 0;
                }
            }

            // mendapatkan index alpha dengan nilai mape terkecil
            $indexMapeTerkecil = array_search(min($MAPE), $MAPE);
            $bestAlphaIndex = $alpha[$indexMapeTerkecil];

            // menyatukan semua hasil perhitungan dan menginputkan hasil peramalan periode berikutnya
            $hasil = array();
            $hasiltotal = 0.0;
            for($j = 0; $j < count($alpha); $j++) {
                for ($i = 0; $i < count($periode); $i++) {
                    if ($i < count($periode)-$bulan) {
                        $hasil[$j][$i] = [
                            'id_bahanbaku' => $id_bahanbaku,
                            'periode'      => $periode[$i],
                            'aktual'       => $X[$i],
                            'hasil'        => $F[$j][$i],
                            'alpha'         => $alpha[$j],
                            'PE'           => $PE[$j][$i],
                            'MAPE'         => $MAPE[$j]
                        ];
                    } else {
                        $hasil[$j][$i] = [
                            'id_bahanbaku' => $id_bahanbaku,
                            'periode'      => $periode[$i],
                            'aktual'       => $X[$i],
                            'hasil'        => $F[$j][$i],
                            'alpha'         => $alpha[$j],
                            'PE'           => $PE[$j][$i],
                            'MAPE'         => $MAPE[$j]
                        ];
                        $total[$i] = $F[$indexMapeTerkecil][$i];
                    }
                }
            }

            $hasiltotal = array_sum($total);
            //end perhitungan SES

            //mencari standar deviasi dan safety stock
            $arr = array();

            function STDV($a){
                $n = count($a);
                $mean = array_sum($a) / $n;
                $variance = 0.0;

                foreach ($a as $val) {
                    $d = ((double) $val) - $mean;
                    $variance += $d * $d;
                };

                return (float)sqrt($variance/($n-1));
            }

            for ($j=0; $j < count($periode); $j++){
                if ($j < (count($periode)-1)){
                    $arr[$j] = $X[$j];
                }
                if ($j == 0){
                    $arr[$j] = $X[0];
                }
            }

            $stdv = STDV($arr);
            $ss = 1.65*$stdv;

            return view('admin/hasilperamalan', compact('hasil', 'bestAlphaIndex','indexMapeTerkecil','alpha', 'periode', 'MAPE', 'bahanbaku', 'bulan', 'tahun', 'ss', 'hasiltotal', 'bb', 'bln'));
        }
    }

    public function simpanperamalan(request $request){
//        for($i=0; $i< count($_POST['bahanbaku_id']); $i++){
//            $peramalan = Peramalan::insert(
//              ['bahanbaku_id' => $_POST['bahanbaku_id'][$i],
//                'tanggal_peramalan' => $_POST['tanggal_peramalan'][$i],
//                'periode' => $_POST['periode'][$i],
//                'alpha' => $_POST['alpha'][$i],
//                'aktual' => $_POST['aktual'][$i],
//                'hasil_peramalan' => $_POST['hasil_peramalan'][$i],
//                'PE' => $_POST['PE'][$i],
//                'MAPE' => $_POST['MAPE'][$i],
//                'safety_stock' => $_POST['safety_stock'][$i]]
//            );
//        }
        $peramalan = new Peramalan();
        $peramalan->bahanbaku_id = $request->bahanbaku_id;
        $peramalan->tanggal_peramalan = $request->tanggal_peramalan;
        $peramalan->periode = $request->periode;
        $peramalan->alpha = $request->alpha;
        $peramalan->aktual = $request->aktual;
        $peramalan->hasil_peramalan = $request->hasil_peramalan;
        $peramalan->PE = $request->PE;
        $peramalan->MAPE = $request->MAPE;
        $peramalan->safety_stock = $request->safety_stock;
        $peramalan->save();

        if ($peramalan){
            return redirect('/peramalan')->with('alert','Data peramalan berhasil ditambah');
        }else{
            return redirect('/peramalan')->with('error','Data peramalan gagal ditambah');
        }
    }

    public function hapusperamalan($id){
        $delete = Peramalan::deletePeramalan($id);
        if ($delete) {
            return redirect('/peramalan')->with('alert','Data peramalan berhasil dihapus');
        } else {
            return redirect('/peramalan')->with('error','Data peramalan gagal dihapus');
        }
    }
}
