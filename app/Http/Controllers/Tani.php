<?php

namespace App\Http\Controllers;

use App\DetailTransaksi;
use App\Permintaan;
use App\Setting;
use App\Transaksi;
use Illuminate\Http\Request;
use App\Stok;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
class Tani extends Controller
{
    //
    public function __construct(){
        $this->middleware(function($request, $next){
            if(Session::get("status")!=2){
                return redirect()->back();
            }
            return $next($request);
        });
    }

    public function homeTani(){
        return view('tani/berandamitra');
    }

    public function gudangTani(){
        $stok = Stok::stokBahanBakuTani(Session::get('id_gudang'));
        $permintaan = Permintaan::getAllPermintaan();
        $jagung = Stok::getStokAdminById(1);
        $padi = Stok::getStokAdminById(2);
        $tebu = Stok::getStokAdminById(3);
        $nilai = Setting::getSetting();

        return view('tani/gudanglimbahtaniedwin',compact('stok','permintaan','jagung','padi','tebu','nilai'));
    }

    public function tambahStok(Request $request){
        DB::table('stok')->where('bahanbaku_id',$request->idbb)
            ->where('gudang_id','=',Session::get('id_gudang'))
            ->update([
                'jumlah'=> DB::raw('jumlah+'.$request->tambahan)
            ]);
        return redirect('/gudangTani');
    }

    public function kirimBB(Request $request){
        if(count($request->all()) == 1){
            return redirect('/gudangTani')->with('error','Belum ada data limbah yang akan dikirim');
        }
        DB::beginTransaction();
        $trans = new Transaksi();
        $trans->tanggal_transaksi = date('Y-m-d');
        $trans->gudang_id = Session::get('id_gudang');
        $trans->status = 1; //status 1 itu tani ngirim ke admin
        $trans->save();
        $idtrans = $trans->id;
        for($i=0;$i<count($request->idbb);$i++){
            $idnya = $request->idbb[$i];
            $qty = $request->qty[$i];
            $satuan = $request->sat[$i];

            $semuakolom = DB::table('stok')->where('bahanbaku_id',$idnya)->where('gudang_id',Session::get('id_gudang'))->first();
            $qtydidb = $semuakolom->jumlah;
            if($qty > $qtydidb){
                DB::rollBack();
                return redirect('/gudangTani')->with('error', 'Data gagal ditambahkan, mohon periksa kembali jumlah pemesanan');
            }else{
                $detail = new DetailTransaksi();
                $detail->bahanbaku_id = $idnya;
                $detail->jumlah = $qty;
                $detail->satuan = $satuan;
                $detail->transaksi_id = $idtrans;
                $detail->save();

                DB::table('stok')->where('bahanbaku_id',$idnya)->where('gudang_id',Session::get('id_gudang'))->update(['jumlah'=>DB::raw('jumlah-'.$qty)]);
            }
        }
        DB::commit();
        return redirect('/gudangTani')->with('alert','Limbah tani berhasil dikirim');
    }

    public function riwayatPengiriman(){
        $jumlah = Transaksi::jumlahRiwayatPengirimanTani(Session::get('id_gudang'));
        $allid = Transaksi::getAllIdTransaksiTani(Session::get('id_gudang'));

        for ($i=1;$i<$jumlah+1;$i++){
            $jumlahdetail = Transaksi::getJumlahDetailTransaksi($allid[$i-1]->id_transaksi);
            for ($j=0;$j<$jumlahdetail;$j++){
                $data["trans".$allid[$i-1]->id_transaksi] = array(
                    "tanggal"=> $allid[$i-1]->tanggal_transaksi,
                    "status"=> $allid[$i-1]->status,
                    "trans"=> Transaksi::getDetailById($allid[$i-1]->id_transaksi)
                );
            }
        }
        $a="";
        if(isset($data)) {
            $a = response()->json($data);
        }
        return view('tani/riwayatpengiriman',compact('a'));
    }
}
