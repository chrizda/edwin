<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use Illuminate\Support\Facades\Session;
class User extends Controller
{
    public function index(){
        if(!Session::get('login')){
            return redirect('login')->with('alert','Kamu harus login dulu');
        }
        else{
            return view('user');
        }
    }

    public function halamanlogin(){
        return view('login');
    }

    public function login(Request $request){
        $u = $request->username;
        $p = $request->password;

        $data = Users::where('username',$u)
            ->where('is_deleted',0)
            ->join('gudang','user_id','=','id')
            ->first();
        if($data){
            if($p == $data->password){
                Session::put('id_gudang',$data->id_gudang);
                Session::put('id_user',$data->id);
                Session::put('username',$data->username);
                Session::put('name',$data->name);
                Session::put('status',$data->status);
                Session::put('foto',$data->foto);

                if($data->status == 1){
                    return redirect('/homeAdmin');
                }else{
                    return redirect('/gudangTani');
                }
            }else{
                return redirect('/halamanlogin')->with('alert','Username/Password salah');
            }
        }else{
            return redirect('/halamanlogin')->with('alert','Username/Password salah');
        }
    }

    public function logout(){
        Session::flush();
        return redirect('/halamanlogin');
    }
}
