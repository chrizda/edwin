<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Transaksi extends Model  {

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'transaksi';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id_transaksi', 'bahanbaku_id', 'tanggal_transaksi', 'jumlah', 'gudang_id', 'status'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['tanggal_transaksi'];

    public $timestamps=false;

    public static function jumlahRiwayatPengirimanAdmin(){
        $trans = DB::table('transaksi')
            ->where('transaksi.status','=','9')
            ->count();

        return $trans;
    }

    public static function getAllIdTransaksi(){
        $idnya = DB::table('transaksi')
            ->select('tanggal_transaksi','id_transaksi')
            ->where('status','=',9)
            ->get();

        return $idnya;
    }

    public static function getDetailById($id){
        $detail = DB::table('detail_transaksi')
            ->where('transaksi_id',$id)
            ->join('bahanbaku','bahanbaku_id','=','id_bahanbaku')
            ->get()->toBase();

        return $detail;
    }

    public static function getTransDetailById($id){
        $detail = DB::table('detail_transaksi')
            ->where('transaksi_id',$id)
            ->join('transaksi','transaksi_id','=','id_transaksi')
            ->get()->toBase();

        return $detail;
    }

    public static function getJumlahDetailTransaksi($id){
        $jumlah = DB::table('detail_transaksi')
            ->where('transaksi_id',$id)
            ->count();
        return $jumlah;
    }

    //tani

    public static function jumlahRiwayatPengirimanTani($idgudang){
        $trans = DB::table('transaksi')
            ->where('gudang_id',$idgudang)
            ->count();

        return $trans;
    }

    public static function getAllIdTransaksiTani($idgudang){
        $idnya = DB::table('transaksi')
            ->select('tanggal_transaksi','id_transaksi','status')
            ->where('gudang_id',$idgudang)
            ->get();

        return $idnya;
    }

    //pengiriman bahan baku

    public static function jumlahPengirimanBahanBaku(){
        $trans = DB::table('transaksi')
            ->where('status','=',1)
            ->count();

        return $trans;
    }

    public static function getAllIdPengiriman(){
        $idnya = DB::table('transaksi')
            ->where('status','=',1)
            ->get();

        return $idnya;
    }

    public static function getJumlahPengiriman(){
        $idnya = DB::table('transaksi')
            ->where('status','=',1)
            ->count();

        return $idnya;
    }

    public static function getDetailUser($idgudang){
        $detail = DB::table('transaksi')->where('gudang_id',$idgudang)
            ->join('gudang','gudang_id','=','id_gudang')
            ->join('users','user_id','=','id')
            ->select('name','no_hp','alamat')
            ->first();

        return $detail;
    }

    public static function tambahStokSetelahPengiriman($idbahanbaku,$jumlah){
        DB::table('stok')->where('bahanbaku_id','=',$idbahanbaku)
            ->where('gudang_id','=',1)
            ->update(['jumlah'=>DB::raw('jumlah+'.$jumlah)]);
    }

    public static function getAktualBeranda($idbahanbaku){
        $now = date('Y');

        $data = Transaksi::selectRaw('DATE_FORMAT(tanggal_transaksi, "%Y-%m") as periode, sum(jumlah) as total')
            ->join('detail_transaksi', 'transaksi.id_transaksi', '=', 'detail_transaksi.transaksi_id')
            ->where('bahanbaku_id', $idbahanbaku)
            ->whereRaw("DATE_FORMAT(tanggal_transaksi, '%Y') = '$now'")
            ->groupBy('periode')
            ->get();
        //ini ambil periodenya
        $periode= array();
        $month = date('Y-m', strtotime($now.'-1'));
        $i = 0;
        while(date('Y-m', strtotime($month)) <= date('Y-m', strtotime($now.'-12'))) {
            $periode[$i] = $month;
            $month = date('Y-m', strtotime("+1 month", strtotime(date($month))));
            $i++;
        }

        //ini menghitung total
        $aktual = array();
        for($i=0; $i<count($periode); $i++) {
            for($j=0; $j<count($data); $j++) {
                if($periode[$i] == $data[$j]['periode']){
                    $aktual[$i] = intval($data[$j]['total']);
                    break;
                }else{
                    $aktual[$i] = 0;
                }
            }
        }

        return $aktual;
    }
}
