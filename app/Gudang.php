<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Gudang extends Model  {

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'gudang';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id_gudang', 'user_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    public $timestamps=false;

    public static function getAllIdGudang(){
        $idnya = DB::table('gudang')->where('id_gudang','<>','1')->pluck('id_gudang');
        return $idnya;
    }

    public static function getAllMitra(){
        return DB::table('gudang')->join('users','user_id','=','id')->where('status',2)->get();
    }

}
