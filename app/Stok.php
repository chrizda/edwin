<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Stok extends Model  {

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stok';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id_stok', 'gudang_id', 'bahanbaku_id', 'jumlah'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    public $timestamps=false;


    public static function stokBahanBakuAdmin(){
        $stok = DB::table('stok')->join('gudang','gudang.id_gudang','=','stok.gudang_id')
            ->join('bahanbaku','bahanbaku.id_bahanbaku','=','stok.bahanbaku_id')
            ->where('stok.gudang_id','1')
            ->get();

        return $stok;
    }

    public static function stokBahanBakuTani($id){
        $stok = DB::table('stok')->join('gudang','gudang.id_gudang','=','stok.gudang_id')
            ->join('bahanbaku','bahanbaku.id_bahanbaku','=','stok.bahanbaku_id')
            ->where('stok.gudang_id',$id)
            ->get();

        return $stok;
    }

    public static function getAllStokTaniByUser($id){
        $stok = DB::table('stok')->join('gudang','gudang.id_gudang','=','stok.gudang_id')
            ->join('bahanbaku','bahanbaku.id_bahanbaku','=','stok.bahanbaku_id')
            ->join('users','gudang.user_id','=','users.id')
            ->where('users.id',$id)
            ->get();

        return $stok;
    }

    public static function deleteStok($idbb){
        $del = DB::table('stok')->where('bahanbaku_id',$idbb)
                    ->delete();
        return $del;
    }

    public static function getStokAdminById($id){
        $qty = DB::table('stok')->where('gudang_id',1)
            ->where('bahanbaku_id',$id)
            ->pluck('jumlah')->first();

        return $qty;
    }

}
