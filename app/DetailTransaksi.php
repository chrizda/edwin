<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailTransaksi extends Model  {

    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'detail_transaksi';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['id_detail_transaksi', 'bahanbaku_id', 'jumlah', 'transaksi_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    public $timestamps=false;

}
