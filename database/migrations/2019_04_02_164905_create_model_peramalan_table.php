<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelPeramalanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peramalan', function (Blueprint $table) {
            $table->increments('id_peramalan');
            $table->integer('bahanbaku_id');
            $table->date('tanggal_peramalan');
            $table->string('periode');
            $table->string('alpha');
            $table->decimal('aktual');
            $table->decimal('hasil_peramalan');
            $table->decimal('PE');
            $table->decimal('MAPE');
            $table->decimal('safety_stock');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peramalan');
    }
}
