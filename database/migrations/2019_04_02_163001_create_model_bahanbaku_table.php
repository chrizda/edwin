<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelBahanbakuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bahanbaku', function (Blueprint $table) {
            $table->increments('id_bahanbaku');
            $table->string('nama_bahanbaku',100);
        });

        $data = array(
            array('nama_bahanbaku' => 'Limbah Jagung'),
            array('nama_bahanbaku' => 'Limbah Padi'),
            array('nama_bahanbaku' => 'Limbah Tebu'),
        );

        DB::table('bahanbaku')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bahanbaku');
    }
}
