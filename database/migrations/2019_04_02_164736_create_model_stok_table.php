<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelStokTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stok', function (Blueprint $table) {
            $table->increments('id_stok');
            $table->integer('gudang_id');
            $table->integer('bahanbaku_id');
            $table->integer('jumlah');
            $table->string('satuan');
        });

        $data = array(
            array('gudang_id' => 1,'bahanbaku_id' => 1, 'jumlah' => 0, 'satuan' => 'Kg'),
            array('gudang_id' => 1,'bahanbaku_id' => 2, 'jumlah' => 0, 'satuan' => 'Kg'),
            array('gudang_id' => 1,'bahanbaku_id' => 3, 'jumlah' => 0, 'satuan' => 'Kg')
        );

        DB::table('stok')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stok');
    }
}
