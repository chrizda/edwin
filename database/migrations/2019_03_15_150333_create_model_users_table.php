<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username',100)->unique();
            $table->string('password');
            $table->string('name');
            $table->date('tgl_lahir');
            $table->string('no_hp');
            $table->text('alamat');
            $table->string('foto');
            $table->tinyInteger('status');
            $table->tinyInteger('is_deleted');
            $table->timestamps();
        });

        DB::table('users')->insert(
            array(
                'username'=>'admin',
                'password'=>'admin',
                'name'=>'Edwin',
                'tgl_lahir'=>date('Y-m-d'),
                'no_hp'=>'081081',
                'alamat'=>'Jl. Semeru',
                'foto'=>'mitratani2.jpg',
                'status'=>1,
                'is_deleted'=>0
            )
        );

        DB::table('users')->insert(
            array(
                'username'=>'tani',
                'password'=>'tani',
                'name'=>'Pak Tani',
                'tgl_lahir'=>date('Y-m-d'),
                'no_hp'=>'081081',
                'alamat'=>'Jl. Semeru',
                'foto'=>'mitratani1.jpg',
                'status'=>2,
                'is_deleted'=>0
            )
        );

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
