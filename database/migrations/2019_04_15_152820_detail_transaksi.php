<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DetailTransaksi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_transaksi', function (Blueprint $table) {
            $table->bigIncrements('id_detail_transaksi');
            $table->integer('bahanbaku_id');
            $table->integer('jumlah');
            $table->string('satuan');
            $table->integer('transaksi_id');
        });

        $insert = array(
            //jagung
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'266',
                'satuan'=>'kg',
                'transaksi_id'=>'1'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'278',
                'satuan'=>'kg',
                'transaksi_id'=>'2'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'210',
                'satuan'=>'kg',
                'transaksi_id'=>'3'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'254',
                'satuan'=>'kg',
                'transaksi_id'=>'4'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'295',
                'satuan'=>'kg',
                'transaksi_id'=>'5'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'327',
                'satuan'=>'kg',
                'transaksi_id'=>'6'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'421',
                'satuan'=>'kg',
                'transaksi_id'=>'7'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'453',
                'satuan'=>'kg',
                'transaksi_id'=>'8'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'385',
                'satuan'=>'kg',
                'transaksi_id'=>'9'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'321',
                'satuan'=>'kg',
                'transaksi_id'=>'10'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'288',
                'satuan'=>'kg',
                'transaksi_id'=>'11'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'297',
                'satuan'=>'kg',
                'transaksi_id'=>'12'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'389',
                'satuan'=>'kg',
                'transaksi_id'=>'13'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'350',
                'satuan'=>'kg',
                'transaksi_id'=>'14'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'328',
                'satuan'=>'kg',
                'transaksi_id'=>'15'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'428',
                'satuan'=>'kg',
                'transaksi_id'=>'16'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'450',
                'satuan'=>'kg',
                'transaksi_id'=>'17'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'327',
                'satuan'=>'kg',
                'transaksi_id'=>'18'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'421',
                'satuan'=>'kg',
                'transaksi_id'=>'19'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'432',
                'satuan'=>'kg',
                'transaksi_id'=>'20'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'388',
                'satuan'=>'kg',
                'transaksi_id'=>'21'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'369',
                'satuan'=>'kg',
                'transaksi_id'=>'22'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'272',
                'satuan'=>'kg',
                'transaksi_id'=>'23'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'246',
                'satuan'=>'kg',
                'transaksi_id'=>'24'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'259',
                'satuan'=>'kg',
                'transaksi_id'=>'25'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'284',
                'satuan'=>'kg',
                'transaksi_id'=>'26'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'255',
                'satuan'=>'kg',
                'transaksi_id'=>'27'
            ),
            array(
                'bahanbaku_id'=>'1',
                'jumlah'=>'379',
                'satuan'=>'kg',
                'transaksi_id'=>'28'
            ),

            //Padi
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'641',
                'satuan'=>'kg',
                'transaksi_id'=>'1'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'620',
                'satuan'=>'kg',
                'transaksi_id'=>'2'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'419',
                'satuan'=>'kg',
                'transaksi_id'=>'3'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'382',
                'satuan'=>'kg',
                'transaksi_id'=>'4'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'395',
                'satuan'=>'kg',
                'transaksi_id'=>'5'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'467',
                'satuan'=>'kg',
                'transaksi_id'=>'6'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'482',
                'satuan'=>'kg',
                'transaksi_id'=>'7'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'386',
                'satuan'=>'kg',
                'transaksi_id'=>'8'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'309',
                'satuan'=>'kg',
                'transaksi_id'=>'9'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'483',
                'satuan'=>'kg',
                'transaksi_id'=>'10'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'452',
                'satuan'=>'kg',
                'transaksi_id'=>'11'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'628',
                'satuan'=>'kg',
                'transaksi_id'=>'12'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'756',
                'satuan'=>'kg',
                'transaksi_id'=>'13'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'780',
                'satuan'=>'kg',
                'transaksi_id'=>'14'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'547',
                'satuan'=>'kg',
                'transaksi_id'=>'15'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'429',
                'satuan'=>'kg',
                'transaksi_id'=>'16'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'521',
                'satuan'=>'kg',
                'transaksi_id'=>'17'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'442',
                'satuan'=>'kg',
                'transaksi_id'=>'18'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'325',
                'satuan'=>'kg',
                'transaksi_id'=>'19'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'368',
                'satuan'=>'kg',
                'transaksi_id'=>'20'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'305',
                'satuan'=>'kg',
                'transaksi_id'=>'21'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'521',
                'satuan'=>'kg',
                'transaksi_id'=>'22'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'534',
                'satuan'=>'kg',
                'transaksi_id'=>'23'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'612',
                'satuan'=>'kg',
                'transaksi_id'=>'24'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'721',
                'satuan'=>'kg',
                'transaksi_id'=>'25'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'684',
                'satuan'=>'kg',
                'transaksi_id'=>'26'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'710',
                'satuan'=>'kg',
                'transaksi_id'=>'27'
            ),
            array(
                'bahanbaku_id'=>'2',
                'jumlah'=>'632',
                'satuan'=>'kg',
                'transaksi_id'=>'28'
            )
        );

        DB::table('detail_transaksi')->insert($insert);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_transaksi');
    }
}
