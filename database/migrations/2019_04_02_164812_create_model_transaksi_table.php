<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModelTransaksiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi', function (Blueprint $table) {
            $table->bigIncrements('id_transaksi');
//            $table->integer('bahanbaku_id');
            $table->date('tanggal_transaksi');
//            $table->integer('jumlah');
            $table->integer('gudang_id');
            $table->tinyInteger('status');
        });

        $insert = array(
            array(
                'tanggal_transaksi'=>'2017-01-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2017-02-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2017-03-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2017-04-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2017-05-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2017-06-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2017-07-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2017-08-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2017-09-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2017-10-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2017-11-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2017-12-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
            'tanggal_transaksi'=>'2018-01-20',
            'gudang_id'=>'1',
            'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2018-02-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2018-03-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2018-04-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2018-05-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2018-06-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2018-07-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2018-08-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2018-09-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2018-10-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2018-11-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2018-12-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2019-01-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2019-02-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2019-03-20',
                'gudang_id'=>'1',
                'status'=>'2'
            ),
            array(
                'tanggal_transaksi'=>'2019-04-20',
                'gudang_id'=>'1',
                'status'=>'2'
            )
        );

        DB::table('transaksi')->insert($insert);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi');
    }
}
