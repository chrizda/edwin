<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'User@halamanlogin');
Route::get('/halamanlogin', 'User@halamanlogin');
Route::post('/login', 'User@login');
Route::get('/logout', 'User@logout');


Route::get('/homeAdmin', 'Admin@homeAdmin');
Route::get('/getStokBeranda/{id}','Admin@getStokBeranda')->name('stokBeranda');
Route::get('/getGrafikBeranda/{id}','Admin@getGrafikBeranda')->name('grafikBeranda');
Route::get('/pengiriman', 'Admin@pengiriman');
Route::get('/terimaTransaksi/{id_transaksi}','Admin@terima')->name('terima');
Route::get('/tolakTransaksi/{id_transaksi}','Admin@tolak')->name('tolak');
Route::get('/gudangAdmin', 'Admin@gudang');
Route::get('/deleteBB/{id_transaksi}','Admin@hapusBB')->name('delete');
Route::get('/pengaturanAkun', 'Admin@akun');
Route::get('/akunBaru', 'Admin@akunBaru');
Route::post('/daftarAkun', 'Admin@postAkun');
Route::get('/halamanUbahAkun/{id}','Admin@halamanubahakun')->name('ubahakun');
Route::post('/ubahAkun','Admin@ubahakun');
Route::get('/deleteAkun/{id}','Admin@deleteakun')->name('deleteakun');
Route::get('/riwayatPengirimanAdmin','Admin@riwayat');
Route::post('/tambahStokAdmin','Admin@tambahStok');
Route::post('/tambahBahanBakuAdmin','Admin@tambahBB');
Route::post('/kirimBahanBakuAdmin','Admin@kirimBB');
Route::post('/tambahPermintaan','Admin@tambahPermintaan');
Route::post('/setNilai','Admin@setNilai');
Route::get('/peramalan', 'Admin@peramalan');
Route::post('/buatperamalan', 'Admin@buatperamalan');
Route::post('/simpanperamalan', 'Admin@simpanperamalan');
Route::get('/hapusPeramalan/{id_peramalan}','Admin@hapusperamalan')->name('hapusperamalan');

//Route::get('/homeTani', 'Tani@homeTani');
Route::get('/gudangTani', 'Tani@gudangTani');
Route::post('/tambahStokTani','Tani@tambahStok');
Route::post('/kirimBahanBakuTani','Tani@kirimBB');
Route::get('/riwayatPengiriman', 'Tani@riwayatPengiriman');

Route::get('/home', 'HomeController@index')->name('home');
