-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 04, 2019 at 08:36 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `edwin`
--

-- --------------------------------------------------------

--
-- Table structure for table `bahanbaku`
--

CREATE TABLE `bahanbaku` (
  `id_bahanbaku` int(10) UNSIGNED NOT NULL,
  `nama_bahanbaku` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bahanbaku`
--

INSERT INTO `bahanbaku` (`id_bahanbaku`, `nama_bahanbaku`) VALUES
(1, 'Limbah Jagung'),
(2, 'Limbah Padi'),
(3, 'Limbah Tebu');

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksi`
--

CREATE TABLE `detail_transaksi` (
  `id_detail_transaksi` bigint(20) UNSIGNED NOT NULL,
  `bahanbaku_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `satuan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaksi_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `detail_transaksi`
--

INSERT INTO `detail_transaksi` (`id_detail_transaksi`, `bahanbaku_id`, `jumlah`, `satuan`, `transaksi_id`) VALUES
(1, 1, 266, 'kg', 1),
(2, 1, 278, 'kg', 2),
(3, 1, 210, 'kg', 3),
(4, 1, 254, 'kg', 4),
(5, 1, 295, 'kg', 5),
(6, 1, 327, 'kg', 6),
(7, 1, 421, 'kg', 7),
(8, 1, 382, 'kg', 8),
(9, 1, 241, 'kg', 9),
(10, 1, 283, 'kg', 10),
(11, 1, 224, 'kg', 11),
(12, 1, 297, 'kg', 12),
(13, 1, 389, 'kg', 13),
(14, 1, 350, 'kg', 14),
(15, 1, 328, 'kg', 15),
(16, 1, 428, 'kg', 16),
(17, 1, 450, 'kg', 17),
(18, 1, 327, 'kg', 18),
(19, 1, 421, 'kg', 19),
(20, 1, 539, 'kg', 20),
(21, 1, 488, 'kg', 21),
(22, 1, 369, 'kg', 22),
(23, 1, 272, 'kg', 23),
(24, 1, 246, 'kg', 24),
(25, 1, 328, 'kg', 25),
(26, 1, 284, 'kg', 26),
(27, 1, 292, 'kg', 27);

-- --------------------------------------------------------

--
-- Table structure for table `gudang`
--

CREATE TABLE `gudang` (
  `id_gudang` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gudang`
--

INSERT INTO `gudang` (`id_gudang`, `user_id`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_03_15_150333_create_model_users_table', 1),
(2, '2019_04_02_163001_create_model_bahanbaku_table', 1),
(3, '2019_04_02_164525_create_model_gudang_table', 1),
(4, '2019_04_02_164736_create_model_stok_table', 1),
(5, '2019_04_02_164812_create_model_transaksi_table', 1),
(6, '2019_04_02_164905_create_model_peramalan_table', 1),
(7, '2019_04_15_152820_detail_transaksi', 1);

-- --------------------------------------------------------

--
-- Table structure for table `peramalan`
--

CREATE TABLE `peramalan` (
  `id_peramalan` int(10) UNSIGNED NOT NULL,
  `bahanbaku_id` int(11) NOT NULL,
  `tanggal_peramalan` date NOT NULL,
  `periode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alpha` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aktual` decimal(8,2) NOT NULL,
  `hasil_peramalan` decimal(8,2) NOT NULL,
  `PE` decimal(8,2) NOT NULL,
  `MAPE` decimal(8,2) NOT NULL,
  `safety_stock` decimal(8,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `peramalan`
--

INSERT INTO `peramalan` (`id_peramalan`, `bahanbaku_id`, `tanggal_peramalan`, `periode`, `alpha`, `aktual`, `hasil_peramalan`, `PE`, `MAPE`, `safety_stock`) VALUES
(1, 1, '2019-04-25', '2017-01', '0.9', '266.00', '0.00', '0.00', '0.22', '0.00'),
(2, 1, '2019-04-25', '2017-02', '0.9', '278.00', '239.40', '0.14', '0.22', '0.00'),
(3, 1, '2019-04-25', '2017-03', '0.9', '210.00', '274.14', '0.31', '0.22', '9.90'),
(4, 1, '2019-04-25', '2017-01', '0.9', '266.00', '0.00', '0.00', '0.22', '0.00'),
(5, 1, '2019-04-25', '2017-02', '0.9', '278.00', '239.40', '0.14', '0.22', '0.00'),
(6, 1, '2019-04-25', '2017-03', '0.9', '210.00', '274.14', '0.31', '0.22', '9.90'),
(7, 1, '2019-05-01', '2019-03', '0.7', '292.00', '291.60', '0.00', '0.20', '135.37');

-- --------------------------------------------------------

--
-- Table structure for table `stok`
--

CREATE TABLE `stok` (
  `id_stok` int(10) UNSIGNED NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `bahanbaku_id` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `satuan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stok`
--

INSERT INTO `stok` (`id_stok`, `gudang_id`, `bahanbaku_id`, `jumlah`, `satuan`) VALUES
(1, 1, 1, 0, 'Kg'),
(2, 1, 2, 0, 'Kg'),
(3, 1, 3, 0, 'Kg'),
(4, 2, 1, 0, 'Kg'),
(5, 2, 2, 0, 'Kg'),
(6, 2, 3, 0, 'Kg');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` bigint(20) UNSIGNED NOT NULL,
  `tanggal_transaksi` date NOT NULL,
  `gudang_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `tanggal_transaksi`, `gudang_id`, `status`) VALUES
(1, '2017-01-20', 2, 1),
(2, '2017-02-20', 2, 1),
(3, '2017-03-20', 2, 1),
(4, '2017-04-20', 2, 1),
(5, '2017-05-20', 2, 1),
(6, '2017-06-20', 2, 1),
(7, '2017-07-20', 2, 1),
(8, '2017-08-20', 2, 1),
(9, '2017-09-20', 2, 1),
(10, '2017-10-20', 2, 1),
(11, '2017-11-20', 2, 1),
(12, '2017-12-20', 2, 1),
(13, '2018-01-20', 2, 1),
(14, '2018-02-20', 2, 1),
(15, '2018-03-20', 2, 1),
(16, '2018-04-20', 2, 1),
(17, '2018-05-20', 2, 1),
(18, '2018-06-20', 2, 1),
(19, '2018-07-20', 2, 1),
(20, '2018-08-20', 2, 1),
(21, '2018-09-20', 2, 1),
(22, '2018-10-20', 2, 1),
(23, '2018-11-20', 2, 1),
(24, '2018-12-20', 2, 1),
(25, '2019-01-20', 2, 1),
(26, '2019-02-20', 2, 1),
(27, '2019-03-20', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_lahir` date NOT NULL,
  `no_hp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `is_deleted` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `tgl_lahir`, `no_hp`, `alamat`, `foto`, `status`, `is_deleted`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', 'Edwin', '2019-04-19', '081081', 'Jl. Semeru', 'mitratani2.jpg', 1, 0, NULL, NULL),
(2, 'mitratani1', 'mitra', 'Mitra Tani 1', '1991-06-11', '089094', 'Gang Dolly', '1556971826.jpg', 2, 0, '2019-05-04 05:10:26', '2019-05-04 05:10:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bahanbaku`
--
ALTER TABLE `bahanbaku`
  ADD PRIMARY KEY (`id_bahanbaku`);

--
-- Indexes for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  ADD PRIMARY KEY (`id_detail_transaksi`);

--
-- Indexes for table `gudang`
--
ALTER TABLE `gudang`
  ADD PRIMARY KEY (`id_gudang`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peramalan`
--
ALTER TABLE `peramalan`
  ADD PRIMARY KEY (`id_peramalan`);

--
-- Indexes for table `stok`
--
ALTER TABLE `stok`
  ADD PRIMARY KEY (`id_stok`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bahanbaku`
--
ALTER TABLE `bahanbaku`
  MODIFY `id_bahanbaku` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `detail_transaksi`
--
ALTER TABLE `detail_transaksi`
  MODIFY `id_detail_transaksi` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `gudang`
--
ALTER TABLE `gudang`
  MODIFY `id_gudang` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `peramalan`
--
ALTER TABLE `peramalan`
  MODIFY `id_peramalan` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `stok`
--
ALTER TABLE `stok`
  MODIFY `id_stok` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
